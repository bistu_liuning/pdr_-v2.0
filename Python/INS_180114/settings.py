###########################################################
# brief:个人室内导航程序
# File Name: setting.m
# Project Name:PDR_BISTU
# Author:Nilssion (Liuning 修改)
# Date:20150105
# CopyRight:Openshoe & 高动态导航技术北京市重点实验室
# Description:
###########################################################
import numpy as np
import math
simdata = {}
###########################################################
## 子函数
###########################################################
#
#  funtion g=gravity(lambda,h)
#
# > @brief Function for calculating the magnitude of the local gravity.
# >
# > @details Function for calculation of the local gravity vector based
# > upon the WGS84 gravity model.
# >
# > @param[out]  g          magnitude of the local gravity vector [m/s^2]
# > @param[in]   lambda     latitude [degrees]
# > @param[in]   h          altitude [m]
# >
###########################################################

def gravity(lambda_, h):
    lambda_     = math.pi / 180 * lambda_
    gamma       = 9.780327 * (1 + 0.0053024 * math.sin(lambda_) ** 2 - 0.0000058 * math.sin(2 * lambda_) ** 2)
    g           = gamma - ((3.0877e-6) - (0.004e-6) * math.sin(lambda_) ** 2) * h + (0.072e-12) * h ** 2
    return g


###########################################################
#
#  funtion  u = load_dataset( str_path )
#
# > @brief Function that loads the IMU data set stored in the specfied
# > folder.
# >
# > @details Function that loads the IMU data set stored in the specfied
# > folder. The file should be named ''data_inert.txt''. The data is scaled
# > to SI-units.
# >
# > @param[out]  u          Matrix of IMU data. Each column corresponed to
# > the IMU data sampled at one time instant.
# >
#
###########################################################
def load_dataset():
    global simdata

    file_address = "LG1_09_09011143.dat"  #定位数据文件位置

    ###########################################数据导入##########################################
    f = open(file_address, 'r')             # 打开文件，只读
    ###########################################读取维度##########################################
    lines = 0
    row = 0

    for line in f.readlines():#读取文件行数
        lines += 1
        if lines == 1:
            for str in line:
                if str == ' ':#读取文件列数
                    row += 1

    Row = lines                #储存数据维度
    Line = row

    Data = np.zeros((Row, Line))#创建数据矩阵

    lines = 1

    data1 = ''
    f.close()
    ###########################################读取数据##########################################
    f = open(file_address, 'r')
    i = 0
    j = 0
    for line in f.readlines():  #逐行读取
        for str in line:        #逐字读取
            if str == ' ':     #判断空格
                Data[i, j] = float(data1)#字符串转换成浮点数并储存至 Data 矩阵
                j += 1
                data1 = ''      #清空
                continue
            data1 += str        #字符串拼接
        lines += 1
        i += 1
        j = 0
    DATA_PDR = Data
    for i in range(Row) :
        j = i + 1   #i从0~Row-1，实际需要从1~Row
        if j > 1 :
            simdata['Ts'].append((DATA_PDR[j - 1, 0] - DATA_PDR[j - 2, 0]) * 0.001)#列表最后一位插入
    data_inert = np.zeros((Row, Line))
    data_inert[:, 0:3]  = DATA_PDR[:,4:7]
    data_inert[:, 3:6]  = DATA_PDR[:,1:4]

    imu_scalefactor = 9.80665 #From the Microstrain IMU data sheet
    f_imu               = data_inert[:,0:3].T
    omega_imu           = data_inert[:,3:6].T * math.pi / 180
    u = np.concatenate((f_imu, omega_imu), axis=0)

    return u

###########################################################
# 主函数
###########################################################
#  funtion u=settings()
#> @brief 进行系统设置和惯导数据导入接口设置
#> @param[out]  u 惯导数据，包含x、y、z陀螺仪和角速度、地磁信息
###########################################################

def settings():
    ###########################################################
    # 定义全局变量，全局内的系统仿真参数设置
    ###########################################################
    global simdata
    one_3_1 = np.ones((3,))
    # 计算当地重力幅值
    # 当地高度 [m]
    simdata['altitude']=100
    # 当地纬度 [degrees]
    simdata['latitude']=58
    # 当地加速度矢量值 [m/s^2]
    simdata['g']=gravity(simdata['latitude'],simdata['altitude'])
    # 系统采样时间 [s]
    Ts = []
    simdata['Ts'] = Ts
    simdata['Ts'].insert(1, 1/100)
    # 导入惯性测量单元数据
    u=load_dataset()
    ###########################################################
    # 零速检测参数设置
    ###########################################################
    # 通过更改字符可改变不同的零速检测模式
    # GLRT - Generalized likelihood ratio test
    # MV -  加速度测量方差检测
    # MAG - 加速度测量幅值检测
    # ARE - 角速度测量能量检测
    simdata['detector_type']='GLRT'
    #Standard deviation of the acceleromter noise [m/s^2]. This is used to
    # control the zero-velocity detectors trust in the accelerometer data.
    simdata['sigma_a']      = 5.5
    # Standard deviation of the gyroscope noise [rad/s]. This is used to
    # control the zero-velocity detectors trust in the gyroscope data.
    simdata['sigma_g']      = 0.6 * math.pi / 180
    # Window size of the zero-velocity detector [samples]
    simdata['Window_size']  = 10
    # Threshold used in the zero-velocity detector. If the test statistics are
    # below this value the zero-velocity hypothesis is chosen.
    simdata['gamma']        = 2000
    ###########################################################
    #滤波参数设置
    ###########################################################
    # By defualt the filter uses a 9-state (position perturbation, velocity
    # perturbation, attitude perturbation) state-space model. If sensor biases
    # and scale factors should be included in the state-space model. Set the
    # follwing control variables to true.

    # Variable controlling if the sensor biases should be included as states
    # in the state-space model.
    simdata['biases']       = 'on'

    # Variable controlling if the sensor scale factor should be included as
    # states in the state-space model.
    simdata['scalefactors'] = 'off'

    # Settings for the process noise, measurement noise, and initial state
    # covariance matrices Q, R, and P. All three matices are assumed to be
    # diagonal matrices, and all settings are defined as standard deviations.

    # Process noise for modeling the accelerometer noise (x,y,z platform
    # coordinate axis) and other accelerometer errors [m/s^2].
    simdata['sigma_acc']    = 0.51 * np.array([1, 1, 1]).T

    # Process noise for modeling the gyroscope noise (x,y,z platform coordinate
    # axis) and other gyroscope errors [rad/s].
    simdata['sigma_gyro']   = 0.08 * np.array([1, 1, 1]).T * math.pi / 180 # [rad/s]

    # Process noise for modeling the drift in accelerometer biases (x,y,z
    # platform coordinate axis) [m/s^2].
    simdata['acc_bias_driving_noise']   = 0.01 * one_3_1

    # Process noise for modeling the drift in gyroscope biases (x,y,z platform
    # coordinate axis) [rad/s].
    simdata['gyro_bias_driving_noise']  = 0.01 * math.pi / 180 * one_3_1


    # Pseudo zero-velocity update measurement noise covariance (R). The
    # covariance matrix is assumed diagonal.
    ## 3#
    # simdata.sigma_vel=[0.0001 0.0001 0.0001]      #[m/s]
    # simdata.sigma_w=[0.09 0.09 0.09]
    # simdata.sigma_yaw=0.004
    ## 1#   RRRR
    simdata['sigma_vel']    = np.array([0.05, 0.05, 0.05])     #[m/s]
    simdata['sigma_w']      = np.array([0.5, 0.5, 0.5])
    simdata['sigma_yaw']    = np.array([0.1, 0.1, 0.1])
    simdata['sigma_pos']    = np.array([0.5, 0.5, 0.02])
    simdata['sigma_bia_acc']= np.array([0.008, 0.008, 0.008])
    #simdata.sigma_yaw=[0.006 0.006 0.006
    # simdata.sigma_w=[0.0002 0.0002 0.0002]
    # simdata.sigma_yaw=0.0000001
    # Diagonal elements of the initial state covariance matrix (P).
    simdata['sigma_initial_pos']        = 0.0001 * one_3_1               # Position (x,y,z navigation coordinate axis) [m]
    simdata['sigma_initial_vel']        = 0.0001 * one_3_1               # Velocity (x,y,z navigation coordinate axis) [m/s]
    simdata['sigma_initial_att']        = (math.pi / 180 * np.array([0.01, 0.01, 0.01]).T)      # Attitude (roll,pitch,heading) [rad]
    simdata['sigma_initial_acc_bias']   = 0.03 * one_3_1           # Accelerometer biases (x,y,z platform coordinate axis)[m/s^2]
    simdata['sigma_initial_gyro_bias']  = 0.2 * math.pi / 180 * one_3_1   # Gyroscope biases (x,y,z platform coordinate axis) [rad/s]
    simdata['sigma_initial_acc_scale']  = 0.0001 * one_3_1       # Accelerometer scale factors (x,y,z platform coordinate axis)
    simdata['sigma_initial_gyro_scale'] = 0.00001 * one_3_1     # Gyroscope scale factors (x,y,z platform coordinate axis)

    # Bias instability time constants [seconds].
    simdata['acc_bias_instability_time_constant_filter']    =1E11
    simdata['gyro_bias_instability_time_constant_filter']   =1E11

    return u
