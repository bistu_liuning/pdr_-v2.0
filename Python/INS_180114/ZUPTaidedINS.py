##########################################################################
#
#> @file ZUPTaidedINS.m
#>
#> @brief This file contains all the functions needed to implement a zero-
#> velocity aided inertial navigation system.
#>
#> @details This file contains all the functions needed to implement a
#> zero-velocity aided inertial navigation system, given a set of IMU data
#> and a vector that indicates when the system has zero-velocity.
#>
#> @authors Isaac Skog, John-Olof Nilsson
#> @copyright Copyright (c) 2011 OpenShoe, ISC License (open source)
#
##########################################################################
import numpy as np
import math
import settings
simdata = settings.simdata

## MAINFUNCTION

##########################################################################
#
#  funtion [x_h cov]=ZUPTaidedINS(u,zupt)
#
#> @brief Function that runs the zero-velocity aided INS Kalman filter
#> algorithm.
#>
#> @details Function that runs the zero-velocity aided INS Kalman filter
#> algorithm. All settings for the filter is done in setting.m.
#>
#> @param[out]  x_h     Matrix with the estimated navigation states. Each row holds the [position, velocity, attitude, (biases, if turned on),(scale factors, if turned on)] for time instant k, k=1,...N.
#> @param[out]  cov     Matrix with the diagonal elements of the state covariance matrices.
#> @param[in]   u       The IMU data vector.
#> @param[in]   zupt    Vector with the decisions of the zero-velocity.
#>
#
##########################################################################
obs = []
def ZUPTaidedINS(u, zupt):
#    x_h = 0
#    cov = 0
    angle=np.zeros((3,3))
    yaw1 = 0
    global m
    global obs
    ##############################################
    ##      Initialize the data fusion          ##
    ##############################################

    # Get  the length of the IMU data vector
    N=max(u.shape)
    obs = np.zeros((N,15))
    m = np.zeros((1,N))
    # Initialize the filter state covariance matrix P, the processes noise
    # covariance matrix Q, the pseudo measurement noise covariance R, and the
    # observation matrix H.
    [P, Q, R, H] = init_filter()        # Subfunction located further down in the file.
    P = np.array(P)
    Q = np.array(Q)
    R = np.array(R)
    H = np.array(H)
    # Allocate vecors
    [x_h, cov, Id] = init_vec(N,P)     # Subfunction located further down in the file.
    # Initialize the navigation state vector x_h, and the quaternion vector
    # quat.
    [x_h[0:9,], quat] = init_Nav_eq(u)  # Subfunction located further down in the file.
    quat = np.array(quat)
    ##############################################
    ##        Run the filter algorithm          ##
    ##############################################
    aaaa = 0
    for k in range(2, N + 1): #=2:N
        ##################################
        #           Time  Update         #
        ##################################
        # Compensate the IMU measurements with the current estimates of the
        ## sensor errors
        aaaa = aaaa + 1
        u_h = comp_imu_errors(u[:, k-1], x_h[:, k - 2]) # Subfunction located further down in the file.

        # Update the navigation equations.

        [x_h[:, k - 1], quat]=Navigation_equations(x_h[:, k - 2], u_h, quat, k) # Subfunction located further down in the file.
        # Update state transition matrix
        [F, G] = state_matrix(quat, u_h, k) # Subfunction located further down in the file.
        F = np.array(F)
        G = np.array(G)

        # Update the filter state covariance matrix P.
        P = F.dot(P.dot(F.T)) + G.dot(Q.dot(G.T))

        # Make sure the filter state covariance matrix is symmetric.
        P = (P + P.T)/2

        # Store the diagonal of the state covariance matrix P.
        cov[:, k - 1] = np.diag(P)

        ##################################
        #     Zero - velocity update     #
        ##################################

        # Check if a zero velocity update should be done. If so, do the
        # following

        window = 10
        delta = 0.15
        d = 0
        aa = 0
        bb = 0

        px = 0
        py = 0
        pz = 0

        if zupt[0,k - 1] == 1 :
            # Calculate the Kalman filter gain

            K = (P.dot(H.T)).dot(np.linalg.inv(H.dot(P.dot(H.T)) + R))  #求逆

            # Calculate the prediction error. Since the detector hypothesis
            # is that the platform has zero velocity, the prediction error is
            # equal to zero minu the estimated velocity.
            z = np.zeros((1,15))
            z[:,3: 6] = - x_h[3 : 6, k - 1] # 速度误差观测量
            z[0, 9]   = np.mean(u[0, 49:100]) - u[0, k - 1] # 加速度测量误差观测量
            z[0,10]   = np.mean(u[1, 49:100]) - u[1, k - 1] # 加速度测量误差观测量
            z[0,11]   = np.mean(u[2, 49:100]) - u[2, k - 1] # 加速度测量误差观测量
            z[0,12]   = np.mean(u[3, 49:100]) - u[3, k - 1] # 陀螺仪测量误差观测量
            z[0,13]   = np.mean(u[4, 49:100]) - u[4, k - 1] # 陀螺仪测量误差观测量
            z[0,14]   = np.mean(u[5, 49:100]) - u[5, k - 1] # 陀螺仪测量误差观测量

            ## 此处增加
            if (k > window) :
                if (zupt[0,(k - window - 1)] == 1) :
                    aa   = np.mean(x_h[6, k-window - 1])-x_h[6, k - 1]
                    bb   = np.mean(x_h[7, k-window - 1])-x_h[7, k - 1]
                    m[0, k - 1] = np.mean(x_h[8, k-window - 1])-x_h[8, k - 1]
                    px   = np.mean(x_h[0, k-window - 1])-x_h[0, k - 1]
                    py   = np.mean(x_h[1, k-window - 1])-x_h[1, k - 1]
                    pz   = np.mean(x_h[2, k-window - 1])-x_h[2, k - 1]
                    # if (abs(m(k)) < delta)
                    # d=m(k)
                    # else
                    # d=0
                    # end
                    d    =  m[0, k - 1]
                    z[0,6] =- aa
                    z[0,7] =- bb
                    z[0,8] =- d # 姿态角观测误差

                    z[0,0] =- px
                    z[0,1] =- py
                    z[0,2] =- pz
                else :
                    z[0,6: 9] = 0
                    z[0,0: 3] = 0

            # #
            # Estimation of the perturbations in the estimated navigation
            # states
            dx = K.dot(z.T)
            # Correct the navigation state using the estimated perturbations.
            # (Subfunction located further down in the file.)
            [x_h[:, k - 1], quat, yaw1]=comp_internal_states(x_h[:, k - 1], dx, quat, angle) # Subfunction located further down in the file.

            # Update the filter state covariance matrix P. \
            P = (Id - K * H) * P

            # Make sure the filter state covariance matrix is symmetric.
            P = (P + P.T)/2

            # Store the diagonal of the state covariance matrix P.
            cov[:, k - 1] = np.diag(P)
            obs[k - 1, :] = z.T[:,0]
        else :
            angle = x_h[6:9, k - 1]
    return [x_h, cov]

## SUBFUNCTIONS

##########################################################################
#
# funtion[P Q R H]=init_filter()
#
# > @brief Function that initializes the Kalman filter.
# >
# > @details Function that initializes the Kalman filter. That is, the
# > function generates the initial covariance matrix P, the process noise
# > covariance matrix Q, the measurement noise covariance matrix R, and
# > observation matrix H, based upon the settings defined in the function
# > settings.m
# >
# > @param [out] P Initial state covariance matrix.
# > @param [out] Q Process noise covariance matrix.
# > @param [out] R Measurement noise covariance matrix.
# > @param [out] H Measurement observation matrix.
# >
##########################################################################
def init_filter():

    global simdata

    # Check which errors that are included in the state space model and
    # allocate P, Q, and H matrices with the right size.

    if (simdata['scalefactors'] ==  'on' and simdata['biases'] == 'on') :  # Both scale and bias errors included

        # Initial state covariance matrix
        P = np.zeros((9 + 6 + 6, 21))
        P[9: 12, 9: 12]     = np.diag(simdata['sigma_initial_acc_bias'] ** 2)
        P[12: 15, 12: 15]   = np.diag(simdata['sigma_initial_gyro_bias'] ** 2)
        P[15: 18, 15: 18]   = np.diag(simdata['sigma_initial_acc_scale'] ** 2)
        P[18: 21, 18: 21]   = np.diag(simdata['sigma_initial_gyro_scale'] ** 2)

        Q = np.zeros((12,12))
        Q[6: 9, 6: 9]       = np.diag(simdata['acc_bias_driving_noise'] ** 2)
        Q[9: 12, 9: 12]     = np.diag(simdata['gyro_bias_driving_noise'] ** 2)

        H = np.zeros((3, 9 + 6 + 6))

    elif (simdata['scalefactors'] == 'on' and simdata['biases'] == 'off'): # Scale errors included

        # Initial state covariance matrix
        P = np.zeros((9 + 6, 15))
        P[9: 12, 9: 12]     = np.diag(simdata['sigma_initial_acc_scale'] ** 2)
        P[12: 15, 12: 15]   = np.diag(simdata['sigma_initial_gyro_scale'] ** 2)

        # Process noise covariance matrix
        Q = np.zeros((6,6))

        # Observation matrix
        H = np.zeros((3, 9 + 6))

    elif (simdata['scalefactors'] == 'off'  and simdata['biases'] == 'on'): # Bias errors included

        # Initial state covariance matrix
        P = np.zeros((9 + 6, 15))
        a = np.diag(simdata['sigma_initial_acc_bias'] ** 2)
        b = simdata['sigma_initial_acc_bias'] ** 2

        P[9: 12, 9: 12]     = np.diag(simdata['sigma_initial_acc_bias'] ** 2)
        P[12: 15, 12: 15]   = np.diag(simdata['sigma_initial_gyro_bias'] ** 2)

        # Process noise covariance matrix
        Q = np.zeros((12,12))
        Q[6: 9, 6: 9]       = np.diag(simdata['acc_bias_driving_noise'] ** 2)
        Q[9: 12, 9: 12]     = np.diag(simdata['gyro_bias_driving_noise'] ** 2)

        # Observation matrix
        H = np.zeros((15, 9 + 6))
    else : # Only the standard errors included

        # Initial state covariance matrix
        P = np.zeros((9,9))

        # Process noise covariance matrix
        Q = np.zeros((6,6))

        # Observation matrix
        H = np.zeros((3, 9))


    # General values for the observation matrix H
    H[0: 3,   0: 3]     = np.eye(3)
    H[3: 6,   3: 6]     = np.eye(3)
    H[6: 9,   6: 9]     = np.eye(3)
    H[9: 12,  9: 12]    = np.eye(3)
    H[12: 15, 12: 15]   = np.eye(3)

    # General values for the initial covariance matrix P
    P[0: 3, 0: 3]       = np.diag(simdata['sigma_initial_pos'] ** 2)
    P[3: 6, 3: 6]       = np.diag(simdata['sigma_initial_vel'] ** 2)
    P[6: 9, 6: 9]       = np.diag(simdata['sigma_initial_att'] ** 2)

    # General values for the process noise covariance matrix Q
    Q[0: 3, 0: 3]       = np.diag(simdata['sigma_acc'] ** 2)
    Q[3: 6, 3: 6]       = np.diag(simdata['sigma_gyro'] ** 2)

    # General values for the measurement noise matrix R
    R = np.eye(15)
    R[0: 3, 0: 3]       = np.diag(simdata['sigma_pos'] ** 2)
    R[3: 6, 3: 6]       = np.diag(simdata['sigma_vel'] ** 2)
    R[6: 9, 6: 9]       = np.diag(simdata['sigma_yaw'] ** 2)
    R[9: 12, 9: 12]     = np.diag(simdata['sigma_bia_acc'] ** 2)
    R[12: 15, 12: 15]   = np.diag(simdata['sigma_w'] ** 2)

    return [P, Q, R, H]

###########################################################################
#
#  funtion [x_h cov Id]=init_vec(N,P)
#
# >
# > @brief Function that allocates memmory for the output of zero-velocity
# > aided inertial navigation algorithm.
# >
# > @param[out]  x_h     Matrix with the estimated navigation states. Each row holds the [position, velocity, attitude, (biases, if turned on),(scale factors, if turned on)] for time instant k, k=1,...N.
# > @param[out]  cov     Matrix with the diagonal elements of the state covariance matrices.
# > @param[out]  Id      Identity matrix.
# > @param[in]   N       The length of the IMU data vector u, i.e., the number of samples.
# > @param[in]   P       Initial state covariance matrix.
# >
#
############################################################################
def init_vec(N, P):

    global simdata

    # Check which errors that are included in the state space model
    if (simdata['scalefactors'] == 'on' and simdata['biases'] == 'on'):
        # Both scale and bias errors included
        cov = np.zeros((9 + 6 + 6, N))
        x_h = np.zeros((9 + 6 + 6, N))

    elif (simdata['scalefactors'] == 'on' and simdata['biases'] == 'off'):
        # Scale errors included
        cov = np.zeros((9 + 6, N))
        x_h = np.zeros((9 + 6, N))

    elif (simdata['scalefactors'] == 'off' and simdata['biases'] == 'on'):
        # Bias errors included
        cov = np.zeros((9 + 6, N))
        x_h = np.zeros((9 + 6, N))
    else :
        # Only the standard errors included
        cov = np.zeros((9, N))
        x_h = np.zeros((9, N))

    Id = np.eye(max(P.shape))
    cov[:, 0] = np.diag(P)

    return [x_h, cov, Id]

###########################################################################
#
#  funtion [x quat]=init_Nav_eq(u)
#
#> @brief Function that calculates the initial state of the navigation
#> equations.
#>
#> @details Function that calculates the initial state of the navigation
#> equations. That is, it does a simple initial alignment of the navigation
#> system, where the roll and pitch of the system is estimated from the
#> 20 first accelerometer readings. All other states are set according to
#> the information given in the function "settings.m".
#>
#> @param[out]  x     Initial navigation state vector.
#> @param[out]  quat  Quaternion vector, representating the initial attitude of the platform.
#> @param[in]   u     Matrix with the IMU data.
#>
#
###########################################################################
def init_Nav_eq(u):

    # Under the assumption that the system is stationary during the first 20
    # samples, the initial roll and pitch is calculate from the 20 first
    # accelerometer readings.
    f_u = np.mean(u[0,0:20])
    f_v = np.mean(u[1,0:20])
    f_w = np.mean(u[2,0:20])

    roll = math.atan2(-f_v, -f_w)
    pitch = math.atan2(f_u, (f_v ** 2 + f_w ** 2) ** 0.5)

    # Set the attitude vector
    attitude = np.zeros((3,1))
    attitude[:,0] = [roll, pitch, 0]
    #attitude=[21.002 13.5709 -34.1013]*pi/180
    #Calculate quaternion corresponing to the initial attitude
    Rb2t = Rt2b(attitude).T
    quat = dcm2q(Rb2t)

    # Set the initial state vector
    x = np.zeros((9,1))
    x[0:3,0] = 0#初始位置设置
    x[6:,] = attitude
    return [x, quat]

###########################################################################
#
#  funtion [y,q]=Navigation_equations(x,u,q)
#
#> @brief The mechanized navigation equations of the inertial navigation
#> system.
#>
#> @details The mechanized navigation equations of the inertial navigation
#> system. That is, the function takes the old state (position, velocity,
#> and attitude) of the navigation system, togheter with the current IMU
#> data measurements (specific force, angular rates), and calculates
#> the current state of the navigation system.
#>
#> @Note The mechanization of the navigation equations that has been
#> implemented is very simple, and several higher order terms has been
#> neglected. Therefore, this mechanization of the navigation equations
#> should only be used in systems using low-cost sensor and where only
#> moderate velocities can be expected.
#>
#> @param[out]   y     New navigation state [position,velocity, attitude (euler angles].
#> @param[out]   q     New quaternions
#> @param[in]    x     Old navigation state
#> @param[in]    u     IMU data [specific force, angular rates].
#> @param[in]    q     Old quaternions
#>
#
###########################################################################
def Navigation_equations(x,u,q,k):

    global simdata

    # Allocate memmory for the output vector
    y=np.zeros((1,max(x.shape)))

    # Get sampling period of the system
    Ts = simdata['Ts'][k - 1]

    #*************************************************************************#
    # Update the quaternion vector "q"  given the angular rate measurements.
    #*************************************************************************#

    w_tb = u[3:6]

    P = w_tb[0]*Ts
    Q = w_tb[1]*Ts
    R = w_tb[2]*Ts

    OMEGA = np.zeros((4, 4))
    OMEGA[0,0:4] = 0.5 * np.array([0,  R,  -Q, P])
    OMEGA[1,0:4] = 0.5 * np.array([-R, 0,  P,  Q])
    OMEGA[2,0:4] = 0.5 * np.array([Q,  -P, 0,  R])
    OMEGA[3,0:4] = 0.5 * np.array([-P, -Q, -R, 0])

    v=np.linalg.norm(w_tb) * Ts

    if v != 0:
        q = (math.cos(v / 2) * np.eye(4) + 2 / v * math.sin(v / 2) * OMEGA).dot(q)
        q = q / np.linalg.norm(q)

    #*************************************************************************#
    # Use the update quaternion to get attitude of the navigation system in
    # terms of Euler angles.
    #*************************************************************************#

    # Get the roll, pitch and yaw

    Rb2t = q2dcm(q)

    # roll
    y[0,6] = math.atan2(Rb2t[2,1], Rb2t[2,2])

    # pitch
    y[0,7] =- math.atan(Rb2t[2,0] / (1-Rb2t[2,0] ** 2) ** 0.5)

    #yaw
    y[0,8] = math.atan2(Rb2t[1,0], Rb2t[0,0])


    #*************************************************************************#
    # Update position and velocity states using the measured specific force,
    # and the newly calculated attitude.
    #*************************************************************************#

    # Gravity vector
    g_t = np.array([0, 0, simdata['g']]).T

    # Transform the specificforce vector into navigation coordinate frame.
    f_t = q2dcm(q).dot(u[0:3])

    # Subtract (add) the gravity, to obtain accelerations in navigation
    # coordinat system.
    acc_t = f_t + g_t

    # State space model matrices
    A=np.eye(6)
    A[0, 3] = Ts
    A[1, 4] = Ts
    A[2, 5] = Ts

    B=np.concatenate((((Ts ** 2) / 2 * np.eye(3)), (Ts * np.eye(3))), axis = 0)

    # Update the position and velocity estimates.
    y[0,0:6] = A.dot(x[0:6]) + B.dot(acc_t)

    return [y,q]


###########################################################################
#
#  funtion [F G]=state_matrix(q,u)
#
# > @brief Function for calculating the state transition matrix F and
# > the process noise gain matrix G.
# >
# > @details Function for calculating the state transition matrix F and
# > the process noise gain matrix G, given the current orientation of
# > the platform and the specific force vector.
# >
# > @param[out]   F     State transition matrix.
# > @param[out]   G     Process noise gain matrix.
# > @param[in]    u     IMU data [specific force, angular rates].
# > @param[in]    q     Old quaternions
# >
#
###########################################################################
def state_matrix(q, u, k):

    global simdata

    # Convert quaternion to a rotation matrix
    Rb2t = q2dcm(q)

    # Transform measured force to force in
    # the navigation coordinate system.

    f_t = Rb2t.dot(u[0:3])

    # Create a ske symmetric matrix of the specific fore vector
    St = np.array([[0, -f_t[2], f_t[1]],
                   [f_t[2], 0, -f_t[0]],
                   [-f_t[1], f_t[0], 0]])

    # Zero matrix
    O = np.zeros((3, 3))

    # Identity matrix
    I = np.eye(3)

    # Diagonal matrices with the specific fore and angular rate along the
    # diagonals.
    Da = np.diag(u[0:3])
    Dg = np.diag(u[3:6])

    # Correlation constant for accelerometer and gyro biases
    B1 = -1 / simdata['acc_bias_instability_time_constant_filter'] * np.eye(3)
    B2 = -1 / simdata['gyro_bias_instability_time_constant_filter'] * np.eye(3)

    # Check which errors that are included in the state space model
    if (simdata['scalefactors'] == 'on' and simdata['biases'] == 'on'):
        # Both scale and bias errors included
        Fc1 = np.hstack((O, I,  O,   O,  O,         O,              O))
        Fc2 = np.hstack((O, O, St, Rb2t, O,   Rb2t.dot(Da),         O))
        Fc3 = np.hstack((O, O,  O,   O, -Rb2t,      O,        -Rb2t.dot(Dg)))
        Fc4 = np.hstack((O, O,  O,   B1, O,         O,              O))
        Fc5 = np.hstack((O, O,  O,   O,  B2,        O,              O))
        Fc6 = np.hstack((O, O,  O,   O,  O,         O,              O))
        Fc7 = np.hstack((O, O,  O,   O,  O,         O,              O))
        Fc  = np.vstack((Fc1,Fc2,Fc3,Fc4,Fc5,Fc6,Fc7))
        # Noise gain matrix
        Gc1 = np.hstack((O,    O,    O,  O))
        Gc2 = np.hstack((Rb2t, O,    O,  O))
        Gc3 = np.hstack((O,  -Rb2t,  O,  O))
        Gc4 = np.hstack((O,    O,    I,  O))
        Gc5 = np.hstack((O,    O,    O,  I))
        Gc6 = np.hstack((O,    O,    O,  O))
        Gc7 = np.hstack((O,    O,    O,  O))
        Gc  = np.vstack((Gc1,Gc2,Gc3,Gc4,Gc5,Gc6,Gc7))


    elif (simdata['scalefactors'] == 'on' and simdata['biases'] == 'off'):
        # Scale errors included
        Fc1 = np.hstack((O,  I,  O,       O,           O       ))
        Fc2 = np.hstack((O,  O,  St,  Rb2t.dot(Da),    O       ))
        Fc3 = np.hstack((O,  O,  O,       O,      -Rb2t.dot(Da)))
        Fc4 = np.hstack((O,  O,  O,       O,           O       ))
        Fc5 = np.hstack((O,  O,  O,       O,           O       ))
        Fc  = np.vstack((Fc1,Fc2,Fc3,Fc4,Fc5))
        # Noise gain matrix
        Gc1 = np.hstack((O,         O ))
        Gc2 = np.hstack((Rb2t,      O ))
        Gc3 = np.hstack((O,      -Rb2t))
        Gc4 = np.hstack((O,         O ))
        Gc5 = np.hstack((O,         O ))
        Gc  = np.vstack((Gc1, Gc2, Gc3, Gc4, Gc5))

    elif (simdata['scalefactors'] == 'off' and simdata['biases'] == 'on'):
        # Bias errors included
        Fc1 = np.hstack((O,  I,  O,     O,         O ))
        Fc2 = np.hstack((O,  O,  St,    Rb2t,      O ))
        Fc3 = np.hstack((O,  O,  O,     O,      -Rb2t))
        Fc4 = np.hstack((O,  O,  O,     B1,        O ))
        Fc5 = np.hstack((O,  O,  O,     O,         B2))
        Fc  = np.vstack((Fc1, Fc2, Fc3, Fc4, Fc5))
        # Noise gain matrix
        Gc1 = np.hstack((O,        O,      O,  O))
        Gc2 = np.hstack((Rb2t,     O,      O,  O))
        Gc3 = np.hstack((O,      -Rb2t,    O,  O))
        Gc4 = np.hstack((O,        O,      I,  O))
        Gc5 = np.hstack((O,        O,      O,  I))
        Gc  = np.vstack((Gc1, Gc2, Gc3, Gc4, Gc5))
    else:
        Fc1 = np.hstack((O, I,  O))
        Fc2 = np.hstack((O, O, St))
        Fc3 = np.hstack((O, O,  O))
        Fc  = np.vstack((Fc1, Fc2, Fc3))
        # Noise gain matrix
        Gc1 = np.hstack((O,           O ))
        Gc2 = np.hstack((Rb2t,        O ))
        Gc3 = np.hstack((O,        -Rb2t))
        Gc = np.vstack((Gc1, Gc2, Gc3 ))


    # Approximation of the discret time state transition matrices
    F = np.eye(max(Fc.shape)) + simdata['Ts'][k - 1] * Fc
    G = simdata['Ts'][k - 1] * Gc

    return [F, G]

###########################################################################
#
#  function [x_out q_out]=comp_internal_states(x_in,dx,q_in)
#
#> @brief Function that corrects the estimated navigation states with
#> the by the Kalman filter estimated  system perturbations (errors).
#>
#> @details Function that corrects the estimated navigation states with
#> the by the Kalman filter estimated system perturbations (errors). That
#> is, the current position an velocity estimates of the navigation
#> platform is corrected by adding the estimated system perturbations to
#> these states. To correct the orientation state (Euler angles and
#> quaternion vector), the quaternion vector are first converted into a
#> rotation matrix, which then is corrected using the estimated orientation
#> perturbations. The corrected rotation matrix is then transformed back
#> into a quaternion vector, as well as the equivalent vector of Euler
#> angles.
#>
#> @param[out]   x_out     Corrected (posteriori) navigation state vector.
#> @param[out]   q_out     Corrected (posteriori) quaternion vector.
#> @param[in]    x_in      A priori estimated navigation state vector.
#> @param[in]    q_in      A priori estimated quaternion vector.
#> @param[in]    dx        Vector of system perturbations
#>
#
###########################################################################
def comp_internal_states(x_in, dx, q_in, angle):

    # Convert quaternion to a rotation matrix
    R = q2dcm(q_in)

    # Correct the state vector
    x_in.shape = (15,1)
    x_out = x_in + dx

    #
    # # Set the attitude vector
    # attitude=np.array([x_out(7), x_out(8), x_out(9)]).T
    # # Calculate quaternion corresponing to the initial attitude
    # R=Rt2b(attitude).T


    # Correct the rotation matrics

    epsilon = np.zeros((3,1))
    epsilon[:,:] = dx[6:9,:]

    OMEGA   = np.array([[0,             -epsilon[2,0],    epsilon[1,0]  ],
                        [epsilon[2,0],    0,              -epsilon[0,0] ],
                        [-epsilon[1,0],   epsilon[0,0],     0           ]])

    R = (np.eye(3)-OMEGA).dot(R)


    # Get the corrected roll, pitch and heading from the corrected rotation
    # matrix
    x_out[6,0] = math.atan2(R[2, 1],R[2, 2])
    x_out[7,0] = -math.atan(R[2, 0]/math.sqrt(1-R[2, 0] ** 2))
    x_out[8,0] = math.atan2(R[1, 0],R[0, 0])
    yaw1=x_out[8,0]
    # # Set the attitude vector
    # attitude=[x_out(7) x_out(8) x_out(9)]'
    # # Calculate quaternion corresponing to the initial attitude
    # R=Rt2b(attitude)'

    # Calculte the corrected quaternions
    q_out = dcm2q(R)
    x_out.shape = (15,)

    return [x_out, q_out, yaw1]


###########################################################################
#
#  function u_out = comp_imu_errors(u_in,x_h)
#
# > @brief Function that compensats for the artifacts in the IMU
# > measurements with the current estimates of the sensors biases and/or
# > scale factor errors.
# >
# > @details Function that compensats for the artifacts in the IMU
# > measurements with the current estimates of the sensors biases and/or
# > scale factor errors. If the sensor errors are not included in the state
# > space model used in the Kalman filter, no correction/compensation is
# > done.
# >
# > @param[out]   u_out     Corrected/compensated IMU measurements.
# > @param[in]    u_in      Raw IMU measurements.
# > @param[in]    x_h       Navigation state vector, where the last states are the estimated sensor errors.
# >
#
###########################################################################
def comp_imu_errors(u_in, x_h):

    global simdata

    # Check which errors that are included in the state space model

    if (simdata['scalefactors'] == 'on' and simdata['biases'] == 'on'):

        # Both scale and bias errors included
        temp = 1 / (np.ones((6, 1)) - x_h[15:])
        u_out = np.diag(temp) * u_in + x_h[9:14]

    elif (simdata['scalefactors'] == 'on' and simdata['biases'] == 'off'):

        # Scale errors included
        temp = 1 / (np.ones((6, 1)) - x_h[9:])
        u_out = np.diag(temp) * u_in

    elif (simdata['scalefactors'] == 'off' and simdata['biases'] == 'on'):

        # Bias errors included
        u_out = u_in + x_h[9:]

    else:

        # Only the standard errors included
        u_out = u_in
    return u_out

###########################################################################
#
#  function R=q2dcm(q)
#
#>
#> @brief Function that converts a  quaternion vector to a directional
#> cosine matrix (rotation matrix)
#>
#> @param[out]   R      Rotation matrix.
#> @param[in]    q      Quaternion vector.
#>
#
###########################################################################
def q2dcm(q):   # q2dcm函数定义
    p = np.zeros((6, 1))
    q.shape = (4,1)
    p[0:4,0] = q[0:4,0] ** 2

    p[4, 0] = p[1, 0] + p[2, 0]
    if p[0, 0] + p[3, 0] + p[4, 0] != 0:
        p[5, 0] = 2 / (p[0, 0] + p[3, 0] + p[4, 0])

    else:
        p[5, 0] = 0

    R = np.zeros((3, 3))
    R[0, 0] = 1 - p[5, 0] * p[4, 0]
    R[1, 1] = 1 - p[5, 0] * (p[0, 0] + p[2, 0])
    R[2, 2] = 1 - p[5, 0] * (p[0, 0] + p[1, 0])

    p[0, 0] = p[5, 0] * q[0,0]
    p[1, 0] = p[5, 0] * q[1,0]
    p[4, 0] = p[5, 0] * q[2,0] * q[3,0]
    p[5, 0] = p[0, 0] * q[1,0]

    R[0, 1] = p[5, 0] - p[4, 0]
    R[1, 0] = p[5, 0] + p[4, 0]

    p[4, 0] = p[1, 0] * q[3,0]
    p[5, 0] = p[0, 0] * q[2,0]

    R[0, 2] = p[5, 0] + p[4, 0]
    R[2, 0] = p[5, 0] - p[4, 0]

    p[4, 0] = p[0, 0] * q[3,0]
    p[5, 0] = p[1, 0] * q[2,0]

    R[1, 2] = p[5, 0] - p[4, 0]
    R[2, 1] = p[5, 0] + p[4, 0]

    return R

###########################################################################
#
#  function q=dcm2q(R)
#
#>
#> @brief Function that converts a directional cosine matrix (rotation
#> matrix) in to a quaternion vector.
#>
#> @param[out]    q      Quaternion vector.
#> @param[in]   R      Rotation matrix.
#>
#
###########################################################################
def dcm2q(R):   # dcm2q函数定义

    T = 1 + R[0, 0] + R[1, 1] + R[2, 2]

    if T > 10 ^ (-8):

        S = 0.5 / T ** 0.5
        qw = 0.25 / S
        qx = (R[2, 1] - R[1, 2]) * S
        qy = (R[0, 2] - R[2, 0]) * S
        qz = (R[1, 0] - R[0, 1]) * S

    else:

        if (R[0, 0] > R[1, 1]) and (R[0, 0] > R[2, 2]):

            S = ((1 + R[0, 0] - R[1, 1] - R[2, 2]) ** 0.5) * 2 # S = 4 * qx
            qw = (R[2, 1] - R[1, 2]) / S
            qx = 0.25 * S
            qy = (R[0, 1] + R[1, 0]) / S
            qz = (R[0, 2] + R[2, 0]) / S

        elif (R[1, 1] > R[2, 2]):

            S = ((1 + R[1, 1] - R[0, 0] - R[2, 2]) ** 0.5) * 2 # S = 4 * qy
            qw = (R[0, 2] - R[2, 0]) / S
            qx = (R[0, 1] + R[1, 0]) / S
            qy = 0.25 * S
            qz = (R[1, 2] + R[2, 1]) / S

        else:

            S = ((1 + R[2, 2] - R[0, 0] - R[1, 1]) ** 0.5) * 2 # S = 4 * qz
            qw = (R[1, 0] - R[0, 1]) / S
            qx = (R[0, 2] + R[2, 0]) / S
            qy = (R[1, 2] + R[2, 1]) / S
            qz = 0.25 * S

    # Store in vector
    q = np.array([qx, qy, qz, qw]).T

    return q

###########################################################################
#
#  function R=Rt2b(ang)
#
#>
#> @brief Function that calculates the rotation matrix for rotating a
#> vector from coordinate frame t to the coordinate frame b, given a
#> vector of Euler angles.
#>
#> @param[out]  R      Rotation matrix.
#> @param[in]   ang    Euler angles [roll,pitch,heading]
#>
#
###########################################################################
def Rt2b(ang):  # Rt2b函数定义
    cr = math.cos(ang[0])
    sr = math.sin(ang[0])

    cp = math.cos(ang[1])
    sp = math.sin(ang[1])

    cy = math.cos(ang[2])
    sy = math.sin(ang[2])

    return np.array([[cy * cp, sy * cp, -sp],
                     [-sy * cr + cy * sp * sr, cy * cr + sy * sp * sr, cp * sr],
                     [sy * sr + cy * sp * cr, - cy * sr + sy * sp * cr, cp * cr]])
