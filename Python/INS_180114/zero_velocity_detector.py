###########################################################################
#
# > @file zero_velocity_detector.m
# >
# > @brief Functions for implementing different zero-velocity detection
# > algorithms.
# >
# > @details Functions for implementing different zero-velocity detection
# > algorithms, as well as a wrapper function for easy to use purpose. The
# > settings used by the wrapper function "zero_velocity_detector()" is
# > specified in the file \a setting.m. Details about the detectors can be
# > found in papers
# >
# > \li <A href="http://dx.doi.org/10.1109/TBME.2010.2060723">Zero-Velocity Detection -- An Algorithm Evaluation</A>
# > \li <A href="http://dx.doi.org/10.1109/IPIN.2010.5646936">Evaluation of Zero-Velocity Detectors for Foot-Mounted Inertial Navigation Systems</A>
# >
# >
# > @authors Isaac Skog, John-Olof Nilsson
# > @copyright Copyright (c) 2011 OpenShoe, ISC License (open source)
#
###########################################################################
import numpy as np
import math
import settings
simdata = settings.simdata

###########################################################################
#
#  funtion [zupt T] = zero_velocity_detector(u)
#
# >
# > @brief Wrapper function for running the zero-velocity detection
# > algorithms.
# >
# > @details A wrapper function that runs the zero-velocity detection
# > algorithm that was specified in the file \a setting.m.
# >
# > @param[out]  zupt       Vector with the detector decsions. [ true = zero velocity, false = moving]
# > @param[out]  T          The test statistics of the detector
# > @param[in]   u          The IMU data vector.
# >
#
###########################################################################
def zero_velocity_detector(u):
    # Global struct holding the simulation settings
    global simdata
    # Allocate memmory
    zupt = np.zeros((1, max(u.shape)))
    # Run the desired detector type. Each detector return a vector with their
    # calculated test statistics T.
    if simdata['detector_type'] == 'MV':
        T = MV(u)
    elif simdata['detector_type'] == 'MAG':
        T = MAG(u)
    elif simdata['detector_type'] == 'ARE':
        T = ARE(u)
    elif simdata['detector_type'] == 'GLRT':
        T = GLRT(u)
    else:
        print('The choosen detector type not recognized. The GLRT detector is used')
        T = GLRT(u)
    # Check if the test statistics T are below the detector threshold. If so,
    # chose the hypothesis that the system has zero velocity
    # T=medfilt1(T,20)
    T = ave(T, 30)
    W = simdata['Window_size']
    for k in range(1, max(T.shape) + 1):
        if T[k - 1] < simdata['gamma']:
            b = np.ones((1, W))
            zupt[0,k - 1:k + W - 1] = b[0, :]
    # Fix the edges of the detector statistics
    T = [max(T) * np.ones((1, int(W / 2))), T, max(T) * np.ones((1, int(W / 2)))]
    return [zupt, T]

## SUBFUNCTIONS

###########################################################################
#
#  funtion T = GLRT(u)
#
# > @brief Function that runs the generalized likelihood test (SHOE detector).
# >
# > @param[out]  T          The test statistics of the detector
# > @param[in]   u          The IMU data vector.
# >
#
###########################################################################
def GLRT(u):

    global simdata
    g = simdata['g']
    sigma2_a = simdata['sigma_a'] ** 2
    sigma2_g = simdata['sigma_g'] ** 2
    W = simdata['Window_size']
    N = max(u.shape)
    T = np.zeros((1, N - W + 1))
    for k in range(1, N - W + 2):
        ya_m = np.mean(u[0:3, k - 1: k + W - 1], 1)
        for l in range(k, k + W):
            tmp = u[0:3, l -1] - g * ya_m / np.linalg.norm(ya_m)
            #T[0, k - 1] = T[0, k - 1] + np.dot(u[3:6, l - 1].T, u[3:6, l - 1]) / sigma2_g + np.dot(tmp.T, tmp) / sigma2_a
            mid = T[0, k - 1] + (u[3:6, l - 1].T).dot(u[3:6, l - 1]) / sigma2_g + tmp.T.dot(tmp) / sigma2_a
            T[0, k - 1] = mid
    T = T / W
    return T

###########################################################################
#
#  funtion T = MV(u)
#
# > @brief Function that runs the acceleration moving variance detector.
# >
# > @param[out]  T          The test statistics of the detector
# > @param[in]   u          The IMU data vector.
# >
#
###########################################################################
def MV(u):

    global simdata
    sigma2_a = simdata['sigma_a'] ** 2
    W = simdata['Window_size']
    N = max(u.shape)
    T = np.zeros((1, N - W + 1))
    for k in range(1,N - W + 2):
        ya_m = np.mean(u[0:3, k - 1: k + W - 1], 1)
        for l in range(k, k + W):
                tmp = u[0:3, l - 1]-ya_m
                T[0, k - 1] = T[0, k - 1] + np.dot(tmp.T, tmp)
    T = T / (sigma2_a * W)
    return T

###########################################################################
#
#  funtion T = MAG(u)
#
# > @brief Function that runs the acceleration magnitude detector.
# >
# > @param[out]  T          The test statistics of the detector
# > @param[in]   u          The IMU data vector.
# >
#
###########################################################################
def MAG(u):

    global simdata
    g = simdata['g']
    sigma2_a = simdata['sigma_a'] ** 2
    W = simdata['Window_size']
    N = max(u.shape)
    T = np.zeros((1, N - W + 1))
    for k in range(1,N - W + 2):
        for l in range(k, k + W):
            T[0, k - 1] = T[0, k - 1] + (np.linalg.norm(u[0:3, l - 1])-g) ** 2
    T = T / (sigma2_a * W)
    return T

###########################################################################
#
#  funtion T = ARE(u)
#
# > @brief Function that runs the angular rate energy detector.
# >
# > @param[out]  T          The test statistics of the detector
# > @param[in]   u          The IMU data vector.
# >
#
###########################################################################
def ARE(u):

    global simdata
    sigma2_g = simdata['sigma_g'] ** 2
    W = simdata['Window_size']
    N = max(u.shape)
    T = np.zeros((1, N - W + 1))
    for k in range(1,N - W + 2):
        for l in range(k, k + W):
            T[0, k - 1] = T[0, k - 1] + np.linalg.norm(u[3:6, l - 1]) ** 2
    T = T / (sigma2_g * W)
    return T

###########################################################################
def ave(q, n):
    m = max(q.shape)
    window = n
    p = []
    for k in range(1, m + 1):
        if (k < m - window):
            p.append(np.mean(q[0,k - 1:k + window - 1]))
        else:
            p.append(q[0,k - 1])
    return np.array(p)