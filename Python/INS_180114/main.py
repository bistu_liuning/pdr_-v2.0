###########################################################
# brief:个人室内导航程序
# File Name: main.m
# Project Name:PDR_BISTU
# Author:Nilssion (Liuning 修改)
# Date:20150105
# CopyRight:Openshoe
# Description:
###########################################################

## 系统初始化
import numpy as np
import settings
import zero_velocity_detector
import ZUPTaidedINS
import matplotlib.pyplot as plt
import math
import matplotlib as mpl

pi = math.pi
simdata = settings.simdata
global m
global obs
## 导入惯导数据
print('1.导入算法惯性测量单元数据')
u=settings.settings()  #u为输出惯导数据矩阵，
## 运行零速检测
print('2.运行零速检测')
[zupt, T]=zero_velocity_detector.zero_velocity_detector(u)
## 运行kalman滤波
print('3.运行卡尔曼滤波')
[x_h, cov]=ZUPTaidedINS.ZUPTaidedINS(u,zupt)
## 打印误差
spwc = np.sqrt(np.sum((x_h[0:2, -1]) ** 2))
kjwc = np.sqrt(np.sum((x_h[0:3, -1]) ** 2))
print('水平误差 = %0.5g , 空间误差 = %0.5g' %(spwc, kjwc))
## 结果显示
print('4.显示结果')
N=max(cov.shape)

t = np.array(list(np.linspace(0,(N - 1) * simdata['Ts'][0] * 1000, N)))
t = t/1000

mpl.rcParams['xtick.labelsize'] = 12 # 通过rcParams设置全局横纵轴字体大小
mpl.rcParams['ytick.labelsize'] = 12

## Plot the IMU data plt
plt.figure(1) #图1
plt.subplot(2,1,1)  #一共2行，这个图是左往右上往下第1个图,1个图占一行
plt.plot(t,u[0, : ].T,label = 'x-axis') #画图，X,Y
plt.plot(t,u[1, : ].T,label = 'y-axis') #画图，X,Y
plt.plot(t,u[2, : ].T,label = 'z-axis') #画图，X,Y
plt.xlabel('time [s]')      #X 轴名称
plt.ylabel('Specific force [m/s^2]') #Y 轴名称
plt.title('Specific force (accelerometer) measurements') #图名

plt.subplot(2,1,2)
plt.plot(t,u[3, : ].T * 180 / pi, label = 'x-axis')
plt.plot(t,u[4, : ].T * 180 / pi, label = 'y-axis')
plt.plot(t,u[5, : ].T * 180 / pi, label = 'z-axis')
plt.xlabel('time [s]')
plt.ylabel('Angular rate  [deg/s]')
plt.title('Angular rate measurements')
plt.legend()
plt.show()

## Plot the trajectory in the horizontal plane
plt.figure(2)
[Row,Line] = x_h.shape
plt.plot( x_h[1, :], x_h[0, :], label = '实际运动轨迹' )
plt.plot(x_h[1, 0], x_h[0, 0],'rs',label = '起始运动点')
plt.plot(x_h[1,Line - 1], x_h[0, Line - 1],'gs', label = '终止运动点')

plt.title('运动轨迹显示')
plt.xlabel('x [m]')
plt.ylabel('y [m]')
plt.legend()
plt.show()

## Plot the height profile, the speed and when ZUPTs were applied

plt.figure(3)
plt.subplot(3,1,1)
plt.plot(t,-x_h[2,:],'b',label = 'mat')
plt.title('Heigth')
plt.xlabel('time [s]')
plt.ylabel('z [m]')

plt.subplot(3,1,2)
plt.plot(t,np.sqrt(np.sum(x_h[3:6,:] ** 2, axis=0)), 'b', label = 'mat')
plt.title('Speed')
plt.xlabel('time [s]')
plt.ylabel('|v| [m/s]')

plt.subplot(3,1,3)
zupt.shape = len(t)
markerline, stemlines, baseline = plt.stem(t, zupt, '-.')
plt.setp(baseline, 'color', 'r', 'linewidth', 2)
plt.title('Zupt applied')
plt.xlabel('time [s]')
plt.ylabel('on/off')
plt.legend()
plt.show()
