/***************************************************************** 
*  @brief:         
*  @File:          
*  @Project:     
*  @Author:      
*  @Date:         
*  @CopyRight:
*  @Version:     
*  @Description:
*****************************************************************/
#include "Interrupt.h"
#if defined(UART_SIGLE_BYTE_MODE)

static uint16_t uart1_check_valid_count=0;
static uint16_t uart1_rx_count=0;
static uint8_t Buffer_Command[COMMAND_NUM]={0};
static uint8_t  header[HEADER_LENGTH]={0};
#endif
static uint8_t  uart1_check=0;
static uint16_t Current_Command_Id=0;
static uint8_t Command_Flag=Command_InValid;

void ISR_Usart1_Rx_Task(void){
#if defined(UART_SIGLE_BYTE_MODE)
  uint16_t i=0;
  if(Command_Flag == Command_InValid){
    uart1_check=0;
    if(uart1_check_valid_count == 0){
      header[0]=Uart_Control[USART1_ID].RX_Buffer[0];
      for(i=0;i<COMMAND_NUM;i++){
        if(Command_Par_Info[i].Header[0] == header[0]){   //search the fit commande header[0]
          Buffer_Command[uart1_check_valid_count] = i;
          uart1_check_valid_count++;
        }
      }
    }else{

      header[1]=Uart_Control[USART1_ID].RX_Buffer[0];
      for(i=0;i<uart1_check_valid_count;i++){
        /*check the fit command*/
        if(Command_Par_Info[Buffer_Command[i]].Header[1] == header[1]){
          Command_Flag = Command_Valid;
          uart1_check_valid_count=0;              //command valid check reset
          Current_Command_Id=Buffer_Command[i];   //storage the current command id
          Command_Par_Info[Current_Command_Id].Data[0]=header[0];uart1_check=uart1_check+header[0];
          Command_Par_Info[Current_Command_Id].Data[1]=header[1];uart1_check=uart1_check+header[1];
          uart1_rx_count=2;
          Command_Par_Info[Current_Command_Id].Length=100;  //forbid the bug on 170805
          break;
        }else{

        }
      }
      uart1_check_valid_count=0;
    }
  }else{
    Command_Par_Info[Current_Command_Id].Data[uart1_rx_count]=Uart_Control[USART1_ID].RX_Buffer[0];   //get the dma data
    if(uart1_rx_count==4){
      Command_Par_Info[Current_Command_Id].Type=Command_Par_Info[Current_Command_Id].Data[2];
      Command_Par_Info[Current_Command_Id].Length=
      (Command_Par_Info[Current_Command_Id].Data[3]<<8)+Command_Par_Info[Current_Command_Id].Data[4];
    }   
    if(uart1_rx_count<(Command_Par_Info[Current_Command_Id].Length-1)){
      uart1_check = uart1_check +Command_Par_Info[Current_Command_Id].Data[uart1_rx_count];
    }
    else{
      if(uart1_check == Command_Par_Info[Current_Command_Id].Data[uart1_rx_count]){   //check the crc
        /*if the command fit the require then resovle this command*/

        Command_Par_Info[Current_Command_Id].Type=Uart_Control[USART1_ID].RX_Buffer[2];
        //Command_Par_Info[Current_Command_Id].Resovle_Func(); 
        Current_Command_Info = &Command_Par_Info[Current_Command_Id];
        Task_Seque_Par.Activate(TASK_COMMAND_RESOLVE);
        Command_Flag = Command_InValid;
        uart1_check=0;
      }else{
        Command_Flag = Command_InValid;
      }   
    }
    uart1_rx_count++;
  }
#elif defined(UART_MULTI_BYTE_MODE)
  uint16_t i=0,j=0;
  uint16_t length=0;
  uint16_t BiasADD=0,BiasADD1=1; //ADD the bias address
  for(i=0;i<COMMAND_NUM;i++){
    if((Command_Par_Info[i].Header[0] == Uart_Control[USART1_ID].RX_Buffer[BiasADD+0])
      &&(Command_Par_Info[i].Header[1] == Uart_Control[USART1_ID].RX_Buffer[BiasADD+1])){  //whether the command is valid?
   
        length = (Uart_Control[USART1_ID].RX_Buffer[BiasADD+3]<<8)|Uart_Control[USART1_ID].RX_Buffer[BiasADD+4];
        uart1_check=0;

        for(j=0;j<length-1;j++){
          uart1_check=uart1_check+Uart_Control[USART1_ID].RX_Buffer[BiasADD+j];
          Command_Par_Info[i].Data[j] = Uart_Control[USART1_ID].RX_Buffer[BiasADD+j];
          Uart_Control[USART1_ID].RX_Buffer[BiasADD+j]=0;
        }

        if(uart1_check==Uart_Control[USART1_ID].RX_Buffer[BiasADD+length-1]){
          Command_Par_Info[i].Data[length-1]=Uart_Control[USART1_ID].RX_Buffer[BiasADD+length-1];
          Current_Command_Id=i;
          Command_Flag = Command_Valid;
          Command_Par_Info[Current_Command_Id].Type=Uart_Control[USART1_ID].RX_Buffer[BiasADD+2];
          Command_Par_Info[Current_Command_Id].Length=length;
          Current_Command_Info = &Command_Par_Info[Current_Command_Id];
          Task_Seque_Par.Activate(TASK_COMMAND_RESOLVE);
          break;
        }else{
          Command_Flag = Command_InValid;
          break;
        }
      /*add on 170719, the buffer will bias from the first byte*/
    }else if((Command_Par_Info[i].Header[0] == Uart_Control[USART1_ID].RX_Buffer[BiasADD1+0])
      &&(Command_Par_Info[i].Header[1] == Uart_Control[USART1_ID].RX_Buffer[BiasADD1+1])){  //whether the command is valid?
   
        length = (Uart_Control[USART1_ID].RX_Buffer[BiasADD1+3]<<8)|Uart_Control[USART1_ID].RX_Buffer[BiasADD1+4];
        uart1_check=0;

        for(j=0;j<length-1;j++){
          uart1_check=uart1_check+Uart_Control[USART1_ID].RX_Buffer[BiasADD1+j];
          Command_Par_Info[i].Data[j] = Uart_Control[USART1_ID].RX_Buffer[BiasADD1+j];
          Uart_Control[USART1_ID].RX_Buffer[BiasADD1+j]=0;
        }

        if(uart1_check==Uart_Control[USART1_ID].RX_Buffer[BiasADD1+length-1]){
          Command_Par_Info[i].Data[length-1]=Uart_Control[USART1_ID].RX_Buffer[BiasADD1+length-1];
          Current_Command_Id=i;
          Command_Flag = Command_Valid;
          Command_Par_Info[Current_Command_Id].Type=Uart_Control[USART1_ID].RX_Buffer[BiasADD1+2];
          Command_Par_Info[Current_Command_Id].Length=length;
          //Command_Par_Info[Current_Command_Id].Resovle_Func(); 
          Current_Command_Info = &Command_Par_Info[Current_Command_Id];
          Task_Seque_Par.Activate(TASK_COMMAND_RESOLVE);
          break;
        }else{
          Command_Flag = Command_InValid;
          break;
        }
    }
  }
  

#endif
}

void ISR_Usart1_Tx_Task(void){
  //HAL_UART_Transmit_DMA(&huart1, Uart_Control[USART1_ID].TX_Buffer, Uart_Control[USART1_ID].TX_Size);
}

void ISR_Usart3_Rx_Task(void){
  Task_Seque_Par.Activate(TASK_TRANSIT_DEV);
}

void ISR_Usart3_Tx_Task(void){
  //HAL_UART_Transmit_DMA(&huart3, Uart_Control[USART3_ID].TX_Buffer, Uart_Control[USART3_ID].TX_Size);
}
/***************************************************************** 
*  @brief:          
*  @Function: 
*  @inparam:
*  @outparam:  
*  @Author:       
*  @Date:         
*  @CopyRight:
*  @Version:     
*  @Description: the based clock source is 100khz                 
*****************************************************************/
void ISR_Tim1_Task(void){
  System_Work_Info.Time_Int++;
  if((System_Work_Info.Time_Int%500)==0){    //ad sample is 2kHz
    Task_Seque_Par.Activate(TASK_SENSOR_READ);
    Task_Seque_Par.Activate(TASK_DEV_CALI);
    Task_Seque_Par.Activate(TASK_INS_LOAD_DATA);
    Task_Seque_Par.Activate(TASK_INS_BUFFER_DATA);
    if((INS_Nav_Par.AlignData[5]==1)||(System_Work_Info.System_Time>2000)){
      Task_Seque_Par.Activate(TASK_INS_CAL);
      Task_Seque_Par.Activate(TASK_PDR_CAL);
    }else if(INS_Nav_Par.AlignData[5]==0){
      Task_Seque_Par.Activate(TASK_ALIGN);
      Task_Seque_Par.Activate(TASK_PDR_INBUFF); //buffer the zero stance data to solve the zero  error.
    }
//    Task_Seque_Par.Activate(TASK_PDR_CAL);
  }
}
/***************************************************************** 
*  @brief:          
*  @Function: 
*  @inparam:
*  @outparam:  
*  @Author:       
*  @Date:         
*  @CopyRight:
*  @Version:     
*  @Description: the based clock source is 1khz, put the data to the pc
*****************************************************************/
void ISR_Tim6_Task(void){
  /*1ms*/
  System_Work_Info.Time_Inc++;
  if(System_Work_Info.Time_Inc>=Dev_Set_Info.Output_SPS){
    System_Work_Info.Time_Inc=0;
    Task_Seque_Par.Activate(TASK_TRANSMIT_DATA); //load the transmit data to pc
  }
}
/***************************************************************** 
*  @brief:          
*  @Function: 
*  @inparam:
*  @outparam:  
*  @Author:       
*  @Date:         
*  @CopyRight:
*  @Version:     
*  @Description:
*****************************************************************/
void IDLE_Usart1_Rx_Task(void)  
{  
    uint32_t temp=0;  

    if((__HAL_UART_GET_FLAG(&huart1,UART_FLAG_IDLE) != SET)){   
      __HAL_UART_CLEAR_IDLEFLAG(&huart1);  
      HAL_UART_DMAStop(&huart1);  
      temp = huart1.hdmarx->Instance->NDTR;  
      Uart_Control[USART1_ID].Current_Rx_len =  Uart_Control[USART1_ID].RX_Size - temp - 1 ;   
      Uart_Control[USART1_ID].Recive_Over_Flag=1;
      ISR_Usart1_Rx_Task();
      HAL_UART_Receive_DMA(&huart1, Uart_Control[USART1_ID].RX_Buffer, Uart_Control[USART1_ID].RX_Size); 
    }          
} 
/***************************************************************** 
*  @brief:          
*  @Function: 
*  @inparam:
*  @outparam:  
*  @Author:       
*  @Date:         
*  @CopyRight:
*  @Version:     
*  @Description:
*****************************************************************/
void IDLE_Usart3_Rx_Task(void)  
{  
    uint32_t temp;  
  
    if((__HAL_UART_GET_FLAG(&huart3,UART_FLAG_IDLE) != SET))  
    {   
        __HAL_UART_CLEAR_IDLEFLAG(&huart3);  
        HAL_UART_DMAStop(&huart3);  
        temp = huart3.hdmarx->Instance->NDTR;  
        Uart_Control[USART3_ID].Current_Rx_len =  Uart_Control[USART3_ID].RX_Size - temp;   
        Uart_Control[USART3_ID].Recive_Over_Flag=1;
        ISR_Usart3_Rx_Task();
        HAL_UART_Receive_DMA(&huart3, Uart_Control[USART3_ID].RX_Buffer, Uart_Control[USART3_ID].RX_Size); 
    }  
} 

