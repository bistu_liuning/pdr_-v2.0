/***************************************************************** 
*  @brief:         
*  @File:          
*  @Project:     
*  @Author:      
*  @Description:
*****************************************************************/
#include "TransmitData.h"
static uint8_t buffer1[100]={0};
void Load_Data(void){
  System_Work_Info.System_Time        =  (float32_t)System_Work_Info.Time_Int*0.01f;
  Dev_Set_Info.Output_Data[TIME]      =  System_Work_Info.System_Time;
  Dev_Set_Info.Output_Data[DEV_NAME]  =  INS_Cal_Par.dt[0]*1000;
  Dev_Set_Info.Output_Data[SWID]      =  System_Work_Info.Soft_Version;
  Dev_Set_Info.Output_Data[RES1]      =  123;
  Dev_Set_Info.Output_Data[RES2]      =  456;
  /*load the raw value*/
  Dev_Set_Info.Output_Data[GX_R]      =  Dev_Sol_Info[DEV_GX].Raw_Data;
  Dev_Set_Info.Output_Data[GY_R]      =  Dev_Sol_Info[DEV_GY].Raw_Data;
  Dev_Set_Info.Output_Data[GZ_R]      =  Dev_Sol_Info[DEV_GZ].Raw_Data;
  Dev_Set_Info.Output_Data[AX1_R]     =  Dev_Sol_Info[DEV_AX].Raw_Data;
  Dev_Set_Info.Output_Data[AY1_R]     =  Dev_Sol_Info[DEV_AY].Raw_Data;
  Dev_Set_Info.Output_Data[AZ1_R]     =  Dev_Sol_Info[DEV_AZ].Raw_Data;
  Dev_Set_Info.Output_Data[MX_R]      =  Dev_Sol_Info[DEV_MX].Raw_Data;
  Dev_Set_Info.Output_Data[MY_R]      =  Dev_Sol_Info[DEV_MY].Raw_Data;
  Dev_Set_Info.Output_Data[MZ_R]      =  Dev_Sol_Info[DEV_MZ].Raw_Data;
  /*temperature*/
  Dev_Set_Info.Output_Data[GX_T]      =  Dev_Sol_Info[DEV_GX].Temp_Data;
  Dev_Set_Info.Output_Data[GY_T]      =  Dev_Sol_Info[DEV_GY].Temp_Data;
  Dev_Set_Info.Output_Data[GZ_T]      =  Dev_Sol_Info[DEV_GZ].Temp_Data;
  Dev_Set_Info.Output_Data[AX1_T]     =  Dev_Sol_Info[DEV_AX].Temp_Data;
  Dev_Set_Info.Output_Data[AY1_T]     =  Dev_Sol_Info[DEV_AY].Temp_Data;
  Dev_Set_Info.Output_Data[AZ1_T]     =  Dev_Sol_Info[DEV_AZ].Temp_Data;
  Dev_Set_Info.Output_Data[MX_T]      =  Dev_Sol_Info[DEV_MX].Temp_Data;
  Dev_Set_Info.Output_Data[MY_T]      =  Dev_Sol_Info[DEV_MY].Temp_Data;
  Dev_Set_Info.Output_Data[MZ_T]      =  Dev_Sol_Info[DEV_MZ].Temp_Data;
  Dev_Set_Info.Output_Data[TEMP]      =  Sensor_Original_Info.AD_DATA[TEMP_ADC_CH];
  /*rough calibration*/
  Dev_Set_Info.Output_Data[GX_RC]      =  Dev_Sol_Info[DEV_GX].RC_Data;
  Dev_Set_Info.Output_Data[GY_RC]      =  Dev_Sol_Info[DEV_GY].RC_Data;
  Dev_Set_Info.Output_Data[GZ_RC]      =  Dev_Sol_Info[DEV_GZ].RC_Data;
  Dev_Set_Info.Output_Data[AX1_RC]     =  Dev_Sol_Info[DEV_AX].RC_Data;
  Dev_Set_Info.Output_Data[AY1_RC]     =  Dev_Sol_Info[DEV_AY].RC_Data;
  Dev_Set_Info.Output_Data[AZ1_RC]     =  Dev_Sol_Info[DEV_AZ].RC_Data;
  Dev_Set_Info.Output_Data[MX_RC]      =  Dev_Sol_Info[DEV_MX].RC_Data;
  Dev_Set_Info.Output_Data[MY_RC]      =  Dev_Sol_Info[DEV_MY].RC_Data;
  Dev_Set_Info.Output_Data[MZ_RC]      =  Dev_Sol_Info[DEV_MZ].RC_Data;
  /*TN calibration*/
  Dev_Set_Info.Output_Data[GX_TN]      =  Dev_Sol_Info[DEV_GX].TN_Data;
  Dev_Set_Info.Output_Data[GY_TN]      =  Dev_Sol_Info[DEV_GY].TN_Data;
  Dev_Set_Info.Output_Data[GZ_TN]      =  Dev_Sol_Info[DEV_GZ].TN_Data;
  Dev_Set_Info.Output_Data[AX1_TN]     =  Dev_Sol_Info[DEV_AX].TN_Data;
  Dev_Set_Info.Output_Data[AY1_TN]     =  Dev_Sol_Info[DEV_AY].TN_Data;
  Dev_Set_Info.Output_Data[AZ1_TN]     =  Dev_Sol_Info[DEV_AZ].TN_Data;
  Dev_Set_Info.Output_Data[MX_TN]      =  Dev_Sol_Info[DEV_MX].TN_Data;
  Dev_Set_Info.Output_Data[MY_TN]      =  Dev_Sol_Info[DEV_MY].TN_Data;
  Dev_Set_Info.Output_Data[MZ_TN]      =  Dev_Sol_Info[DEV_MZ].TN_Data;
  /*SF calibration*/
  Dev_Set_Info.Output_Data[GX_SF]      =  Dev_Sol_Info[DEV_GX].SF_Data;
  Dev_Set_Info.Output_Data[GY_SF]      =  Dev_Sol_Info[DEV_GY].SF_Data;
  Dev_Set_Info.Output_Data[GZ_SF]      =  Dev_Sol_Info[DEV_GZ].SF_Data;
  Dev_Set_Info.Output_Data[AX1_SF]     =  Dev_Sol_Info[DEV_AX].SF_Data;
  Dev_Set_Info.Output_Data[AY1_SF]     =  Dev_Sol_Info[DEV_AY].SF_Data;
  Dev_Set_Info.Output_Data[AZ1_SF]     =  Dev_Sol_Info[DEV_AZ].SF_Data;
  Dev_Set_Info.Output_Data[MX_SF]      =  Dev_Sol_Info[DEV_MX].SF_Data;
  Dev_Set_Info.Output_Data[MY_SF]      =  Dev_Sol_Info[DEV_MY].SF_Data;
  Dev_Set_Info.Output_Data[MZ_SF]      =  Dev_Sol_Info[DEV_MZ].SF_Data;
  /*CR calibration*/
  Dev_Set_Info.Output_Data[GX_CR]      =  Dev_Sol_Info[DEV_GX].CR_Data;
  Dev_Set_Info.Output_Data[GY_CR]      =  Dev_Sol_Info[DEV_GY].CR_Data;
  Dev_Set_Info.Output_Data[GZ_CR]      =  Dev_Sol_Info[DEV_GZ].CR_Data;
  Dev_Set_Info.Output_Data[AX1_CR]     =  Dev_Sol_Info[DEV_AX].CR_Data;
  Dev_Set_Info.Output_Data[AY1_CR]     =  Dev_Sol_Info[DEV_AY].CR_Data;
  Dev_Set_Info.Output_Data[AZ1_CR]     =  Dev_Sol_Info[DEV_AZ].CR_Data;
  Dev_Set_Info.Output_Data[MX_CR]      =  Dev_Sol_Info[DEV_MX].CR_Data;
  Dev_Set_Info.Output_Data[MY_CR]      =  Dev_Sol_Info[DEV_MY].CR_Data;
  Dev_Set_Info.Output_Data[MZ_CR]      =  Dev_Sol_Info[DEV_MZ].CR_Data;
  /*Attitude caculation value*/
  Dev_Set_Info.Output_Data[THETA]      =  INS_Nav_Par.Att[Pitch]*57.3f;
  Dev_Set_Info.Output_Data[PHI]        =  INS_Nav_Par.Att[Yaw]*57.3f;
  Dev_Set_Info.Output_Data[GAMMA]      =  INS_Nav_Par.Att[Roll]*57.3f;
  Dev_Set_Info.Output_Data[INSPE]      =  INS_Nav_Par.Pos[PosE];
  Dev_Set_Info.Output_Data[INSPN]      =  INS_Nav_Par.Pos[PosN];
  Dev_Set_Info.Output_Data[INSPD]      =  INS_Nav_Par.Pos[PosD];
  Dev_Set_Info.Output_Data[INSVE]      =  INS_Nav_Par.Vel[VelE];
  Dev_Set_Info.Output_Data[INSVN]      =  INS_Nav_Par.Vel[VelN];
  Dev_Set_Info.Output_Data[INSVD]      =  INS_Nav_Par.Vel[VelD];
  /*ADC Value*/
  //Dev_Set_Info.Output_Data[ADC0V]      =  Sensor_Original_Info.AD_DATA[0];
  Dev_Set_Info.Output_Data[ADC0V]      =  PDR_Set_Par.StepNum;
	//Dev_Set_Info.Output_Data[ADC1V]      =  Sensor_Original_Info.AD_DATA[1];
	Dev_Set_Info.Output_Data[ADC1V]      =  PDR_Set_Par.Zupt;
	
	
  Dev_Set_Info.Output_Data[ADC2V]      =  Sensor_Original_Info.AD_DATA[2];
  Dev_Set_Info.Output_Data[ADC3V]      =  Sensor_Original_Info.AD_DATA[3];
  Dev_Set_Info.Output_Data[ADC4V]      =  Sensor_Original_Info.AD_DATA[4];
  Dev_Set_Info.Output_Data[ADC5V]      =  Sensor_Original_Info.AD_DATA[5];
  Dev_Set_Info.Output_Data[ADC6V]      =  Sensor_Original_Info.AD_DATA[6];
  Dev_Set_Info.Output_Data[ADC7V]      =  Sensor_Original_Info.AD_DATA[7];
  Dev_Set_Info.Output_Data[ADC8V]      =  Sensor_Original_Info.AD_DATA[8];
  Dev_Set_Info.Output_Data[ADC9V]      =  Sensor_Original_Info.AD_DATA[9];
  Dev_Set_Info.Output_Data[ADC10V]     =  Sensor_Original_Info.AD_DATA[10];
  Dev_Set_Info.Output_Data[ADC11V]     =  Sensor_Original_Info.AD_DATA[11];
  Dev_Set_Info.Output_Data[ADC12V]     =  MS5611_Temperature;
  Dev_Set_Info.Output_Data[ADC13V]     =  MS5611_Pressure;
  Dev_Set_Info.Output_Data[ADC14V]     =  MS5611_Altitude;
  Dev_Set_Info.Output_Data[ADC15V]     =  Sensor_Original_Info.AD_DATA[15];

  Dev_Set_Info.Output_Data[GX_O]      =  Dev_Sol_Info[DEV_GX].Out_Data;
  Dev_Set_Info.Output_Data[GY_O]      =  Dev_Sol_Info[DEV_GY].Out_Data;
  Dev_Set_Info.Output_Data[GZ_O]      =  Dev_Sol_Info[DEV_GZ].Out_Data;
  Dev_Set_Info.Output_Data[AX1_O]     =  Dev_Sol_Info[DEV_AX].Out_Data;
  Dev_Set_Info.Output_Data[AY1_O]     =  Dev_Sol_Info[DEV_AY].Out_Data;
  Dev_Set_Info.Output_Data[AZ1_O]     =  Dev_Sol_Info[DEV_AZ].Out_Data;
  Dev_Set_Info.Output_Data[MX_O]      =  Dev_Sol_Info[DEV_MX].Out_Data;
  Dev_Set_Info.Output_Data[MY_O]      =  Dev_Sol_Info[DEV_MY].Out_Data;
  Dev_Set_Info.Output_Data[MZ_O]      =  Dev_Sol_Info[DEV_MZ].Out_Data;
  
  Dev_Set_Info.Output_Data[MODE]       =  Dev_Set_Info.Run_Mode;
  Dev_Set_Info.Output_Data[VERSION]    =  SW_VER;

  
}

void Mode_Select_Data(uint16_t Mode){
  uint16_t i=0;

  switch(Mode){
    case(0):{
      /*allocation the define channel*/
      for(i=0;i<Dev_Set_Info.Output_Ch_Num;i++){
        Dev_Set_Info.Send_To_User_Data[i]=Dev_Set_Info.Output_Data[Dev_Set_Info.Output_Ch[i]];
      }
      break;
    }
    case(1):{
      Dev_Set_Info.Send_To_User_Data[0]=System_Work_Info.System_Time;
      Dev_Set_Info.Send_To_User_Data[1]=Sensor_Original_Info.AD_DATA[0];
      Dev_Set_Info.Send_To_User_Data[2]=Sensor_Original_Info.AD_DATA[1];
      Dev_Set_Info.Send_To_User_Data[3]=Sensor_Original_Info.AD_DATA[2];
      Dev_Set_Info.Send_To_User_Data[4]=Sensor_Original_Info.AD_DATA[3];
      Dev_Set_Info.Send_To_User_Data[5]=Sensor_Original_Info.AD_DATA[4];
      Dev_Set_Info.Send_To_User_Data[6]=Sensor_Original_Info.AD_DATA[5];
      Dev_Set_Info.Send_To_User_Data[7]=Sensor_Original_Info.AD_DATA[6];
      Dev_Set_Info.Send_To_User_Data[8]=Sensor_Original_Info.AD_DATA[7];
      Dev_Set_Info.Send_To_User_Data[9]=Sensor_Original_Info.AD_DATA[8];
      Dev_Set_Info.Send_To_User_Data[10]=Sensor_Original_Info.AD_DATA[9];
      Dev_Set_Info.Send_To_User_Data[11]=Sensor_Original_Info.AD_DATA[10];
      Dev_Set_Info.Send_To_User_Data[12]=Sensor_Original_Info.AD_DATA[11];
      Dev_Set_Info.Send_To_User_Data[13]=MS5611_Temperature;
      Dev_Set_Info.Send_To_User_Data[14]=MS5611_Pressure;
      Dev_Set_Info.Send_To_User_Data[15]=MS5611_Altitude;
      Dev_Set_Info.Send_To_User_Data[16]=Sensor_Original_Info.AD_DATA[15];
    
      break;
    }
  }
}

void Transmit_Data_To_PC(void){
  Load_Data();     //load the output data;
  Mode_Select_Data(Dev_Set_Info.Output_Data_Mode);
  /*caculate the ouput command*/
  Uart_Control[USART1_ID].TX_Size =Command_Output_Send(Uart_Control[USART1_ID].TX_Buffer,
                                                       USART1_ID,
                                                       Dev_Set_Info.Output_Prot_Mode);
#if defined(USE_HAL_DRIVER)
//  while(Uart_Control[USART1_ID].Transmit_Over_Flag==DMA_Sending);  //add on 170616, produce a bug
  Uart_Control[USART1_ID].Transmit_Over_Flag = DMA_Sending;
  HAL_UART_Transmit_DMA(&huart1,Uart_Control[USART1_ID].TX_Buffer, Uart_Control[USART1_ID].TX_Size);
  HAL_UART_Transmit_DMA(&huart2,Uart_Control[USART1_ID].TX_Buffer, Uart_Control[USART1_ID].TX_Size);
  if(Dev_Set_Info.Output_SPS>=1000){
    HAL_UART_Transmit_DMA(&huart3,Uart_Control[USART1_ID].TX_Buffer, Uart_Control[USART1_ID].TX_Size);
  }
#elif defined(USE_STDPERIPH_DRIVER)
  
#endif
}

void Transmit_Ask_To_PC(void){
  uint16_t length=0;
  length = Command_ASK_Send(buffer1,Current_Command_Info[0].Header,Current_Command_Info[0].Length);

#if defined(USE_HAL_DRIVER)
//  while(Uart_Control[USART1_ID].Transmit_Over_Flag==DMA_Sending);  //add on 170616, produce a bug
  Uart_Control[USART1_ID].Transmit_Over_Flag = DMA_Sending;  
  HAL_UART_Transmit_DMA(&huart1,buffer1, length);
#elif defined(USE_STDPERIPH_DRIVER)
    
#endif

}

void Transmit_Data_To_Dev(void){
  uint16_t length=33;
  float32_t buffer1[7]={0};
  buffer1[0]=Dev_Set_Info.Output_Data[INSPE];
  buffer1[1]=Dev_Set_Info.Output_Data[INSPN];
  buffer1[2]=Dev_Set_Info.Output_Data[INSPD];
  buffer1[3]=Dev_Set_Info.Output_Data[THETA];
  buffer1[4]=Dev_Set_Info.Output_Data[PHI];
  buffer1[5]=Dev_Set_Info.Output_Data[GAMMA];
  buffer1[6]=0x01;

  length=Command_Modbus_Send(buffer1,Uart_Control[USART3_ID].TX_Buffer,7);
  
#if defined(USE_HAL_DRIVER)
//  while(Uart_Control[USART1_ID].Transmit_Over_Flag==DMA_Sending);  //add on 170616, produce a bug
  Uart_Control[USART3_ID].Transmit_Over_Flag = DMA_Sending;  
  HAL_UART_Transmit_DMA(&huart3,Uart_Control[USART3_ID].TX_Buffer,length);
#elif defined(USE_STDPERIPH_DRIVER)
    
#endif
}






