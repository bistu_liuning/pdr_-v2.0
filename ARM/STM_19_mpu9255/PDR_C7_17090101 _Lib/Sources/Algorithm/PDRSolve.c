/**/
#include "PDRSolve.h"


PDR_Info PDR_Set_Par = { 0, true, true, 0, 0.01f, 0.1f*ATR, 0.3E5f, 3, 0, 0, 0.15, 3, { 0 }, {0},{ 0 } };
PDR_Func_Info PDR_Cal_Func = { &PDRSolve_Init, &PDR_Detector, &PDR_Cal,&PDR_Zero_Buffer};

static float32_t Tk=0;
static float32_t StepFlag = false;

vec3 temp1={0};
float32_t temp2=0,gyro2=0;
void PDR_Storage_Data(void);
void PDR_Detector(void){
  uint16_t i = 0,j=0;
  float32_t g_norm = 0;
  vec3  g_mean = { 0 };
  float32_t sigma2_g=PDR_Set_Par.Sigma_g*PDR_Set_Par.Sigma_g;
  float32_t sigma2_a=PDR_Set_Par.Sigma_a*PDR_Set_Par.Sigma_a;
  
  g_norm = Mat_Cal_Info.Norm(INS_Sensor_Par[Acc].Mean,AXIS_NUM,1);  //ins_sensor_par storage the ins data
  g_mean[AxisX] = INS_Sensor_Par[Acc].Mean[AxisX];
  g_mean[AxisY] = INS_Sensor_Par[Acc].Mean[AxisY];
  g_mean[AxisZ] = INS_Sensor_Par[Acc].Mean[AxisZ];

  Tk=0;
  for(i=0;i<PDR_Set_Par.WindowSize;i++){
    for(j=0;j<AXIS_NUM;j++){
      temp1[j]=INS_Sensor_Par[Acc].BufferData[j][i]-g*g_mean[j]/g_norm;
    }
    gyro2=INS_Sensor_Par[Gyro].BufferData[AxisX][i]*INS_Sensor_Par[Gyro].BufferData[AxisX][i]
          +INS_Sensor_Par[Gyro].BufferData[AxisY][i]*INS_Sensor_Par[Gyro].BufferData[AxisY][i]
          +INS_Sensor_Par[Gyro].BufferData[AxisZ][i]*INS_Sensor_Par[Gyro].BufferData[AxisZ][i];
    temp2=temp1[0]*temp1[0]+temp1[1]*temp1[1]+temp1[2]*temp1[2];
    Tk=Tk+gyro2/sigma2_g+temp2/sigma2_a;
  }
  Tk=Tk/PDR_Set_Par.WindowSize;
//   TkSum = 0;
//   for (i = 0; i <TkNum-1; i++)
//   {
// 	  TkBuffer[TkNum - 1 - i] = TkBuffer[TkNum - 1 - i-1];
// 	  TkSum = TkSum + TkBuffer[TkNum - 1 - i];
//   }
//   TkBuffer[0] = Tk;
//   TkSum = TkSum + Tk;
//   Tk = TkSum / TkNum;

  PDR_Set_Par.Tk = Tk;

  if(Tk<PDR_Set_Par.Gamma){
    PDR_Set_Par.Zupt=true;
    if (StepFlag==false){
      StepFlag = true;
      PDR_Set_Par.StepNum++;
	  PDR_Set_Par.Pos[PosN] = INS_Nav_Par.Data.Mean[PosPar][PosN];
	  PDR_Set_Par.Pos[PosE] = INS_Nav_Par.Data.Mean[PosPar][PosE];
	  PDR_Set_Par.Pos[PosD] = INS_Nav_Par.Data.Mean[PosPar][PosD];
    }
  }else{
    PDR_Set_Par.Zupt=false;
	  StepFlag = false;
  }
  PDR_Storage_Data();
}

void PDR_Storage_Data(void){
	uint16_t i = 0;
	for (i = 0; i < PDR_Set_Par.Buffer.BufferSize-1;i++){
		PDR_Set_Par.Buffer.ZuptBuff[(PDR_Set_Par.Buffer.BufferSize - 1) - i] =
			PDR_Set_Par.Buffer.ZuptBuff[(PDR_Set_Par.Buffer.BufferSize - 1) - i - 1];
	}
	PDR_Set_Par.Buffer.ZuptBuff[0] = PDR_Set_Par.Zupt;
}
