/***************************************************************** 
*  @brief:         
*  @File:          
*  @Project:     
*  @Author:      
*  @Date:       170601
*  @CopyRight:
*  @Version:     
*  @Description:
*****************************************************************/
#include "KFFilter.h"

/*variable define*/
static mat3x3 Id = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };
matDSxDS Fc = { 0 }, tempDSxDS = {0};
matDSxDP Gc = { 0 };
vec3 ft = { 0 };

mat3x3 dcmError = { 0 };
mat3x3 M3temp1 = { 0 }, M3temp2 = { 0 };

Kalman_Cal_Info Kalman_Cal_Func = {&KFFilter_Init,&KFFilter_Cal_State,&KFFilter_State_Update,&KFFilter_Time_Updata,&KFFilter_Measure_Update};
Kalman_Set_Info Kalman_Set_Par = { { 0.5f }, { 0.5f*ATR }, { 1E-7f }, { ATR*1E-7f }, { 0.01f }, {0.01f},{0.0001}, 0.0004f, 0.01f, 1E-5 , { 1E-5f }, { 0.1f*ATR }, { 0.3f },
                                   { 0.3f*ATR }, { 1E-4f }, { 1E-5f }, 0, 0,0};
Kalman_Info Kalman_Par = { { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, { 0 }, {0} };

void KFFilter_Init(void){

#if defined(OPENSHOE) 
	//	Kalman_Set_Par.BuffData = Command_Par_Info[COMMAND_KALMAN].Buffer;
	Kalman_Set_Par.SigmaAcc[0] = 0.51;
	Kalman_Set_Par.SigmaAcc[1] = 0.51;
	Kalman_Set_Par.SigmaAcc[2] = 0.51;

	Kalman_Set_Par.SigmaGyro[0] = 0.08 * ATR;
	Kalman_Set_Par.SigmaGyro[1] = 0.08 * ATR;
	Kalman_Set_Par.SigmaGyro[2] = 0.08 * ATR;

	Kalman_Set_Par.AccDrNoise[0] = 0.01;
	Kalman_Set_Par.AccDrNoise[1] = 0.01;
	Kalman_Set_Par.AccDrNoise[2] = 0.01;

	Kalman_Set_Par.GyroDrNoise[0] = 0.01f*ATR;
	Kalman_Set_Par.GyroDrNoise[1] = 0.01f*ATR;
	Kalman_Set_Par.GyroDrNoise[2] = 0.01f*ATR;

	Kalman_Set_Par.SigmaPos[PosN] = Kalman_Set_Par.SigmaPos[PosE] = 0.5;
	Kalman_Set_Par.SigmaPos[PosD] = 0.5;
	Kalman_Set_Par.SigmaVel[VelN] = Kalman_Set_Par.SigmaVel[VelE] = Kalman_Set_Par.SigmaVel[VelD] = 0.05;
	Kalman_Set_Par.SigmaAtt[Roll] = Kalman_Set_Par.SigmaAtt[Pitch] = Kalman_Set_Par.SigmaAtt[Yaw] = 0.1;

	Kalman_Set_Par.SigmaAccBias[AccX] = Kalman_Set_Par.SigmaAccBias[AccY] = Kalman_Set_Par.SigmaAccBias[AccZ] = 0.008;
	Kalman_Set_Par.SigmaGyroBias[GyroX] = Kalman_Set_Par.SigmaGyroBias[GyroY] = Kalman_Set_Par.SigmaGyroBias[GyroZ] = 0.5;

	Kalman_Set_Par.SigmaInPos[0] = 1E-4;
	Kalman_Set_Par.SigmaInPos[1] = 1E-4;
	Kalman_Set_Par.SigmaInPos[2] = 1E-4;

	Kalman_Set_Par.SigmaInVel[0] = 1E-4;
	Kalman_Set_Par.SigmaInVel[1] = 1E-4;
	Kalman_Set_Par.SigmaInVel[2] = 1E-4;

	Kalman_Set_Par.SigmaInAtt[0] = 0.01f*ATR;
	Kalman_Set_Par.SigmaInAtt[1] = 0.01f*ATR;
	Kalman_Set_Par.SigmaInAtt[2] = 0.01f*ATR;

	Kalman_Set_Par.SigmaInAccBias[0] = 0.03;
	Kalman_Set_Par.SigmaInAccBias[1] = 0.03;
	Kalman_Set_Par.SigmaInAccBias[2] = 0.03;

	Kalman_Set_Par.SigmaInGyroBias[0] = 0.2f*ATR;
	Kalman_Set_Par.SigmaInGyroBias[1] = 0.2f*ATR;
	Kalman_Set_Par.SigmaInGyroBias[2] = 0.2f*ATR;

	// 	Kalman_Set_Par.SigmaAcc[0] = 0.5f; Kalman_Set_Par.SigmaAcc[1] = 0.5f; Kalman_Set_Par.SigmaAcc[2] = 0.5f;
	// 	Kalman_Set_Par.SigmaGyro[0] = 0.5f*ATR; Kalman_Set_Par.SigmaGyro[1] = 0.5f*ATR; Kalman_Set_Par.SigmaGyro[2] = 0.5f*ATR;
	// 	Kalman_Set_Par.AccDrNoise[0] = 1e-7; Kalman_Set_Par.AccDrNoise[1] = 1e-7; Kalman_Set_Par.AccDrNoise[2] = 1e-7;
	// 	Kalman_Set_Par.GyroDrNoise[0] = 0.5f*ATR; Kalman_Set_Par.GyroDrNoise[1] = 0.5f*ATR; Kalman_Set_Par.GyroDrNoise[2] = 0.5f*ATR;
	// 	Kalman_Set_Par.SigmaVel[0] = 0.01; Kalman_Set_Par.SigmaVel[1] = 0.01; Kalman_Set_Par.SigmaVel[2] = 0.01;
	// 
	// 	Kalman_Set_Par.SigmaInPos[0] = 1E-5; Kalman_Set_Par.SigmaInPos[1] = 1E-5; Kalman_Set_Par.SigmaInPos[2] = 1E-5;
	// 	Kalman_Set_Par.SigmaInVel[0] = 1E-5; Kalman_Set_Par.SigmaInVel[1] = 1E-5; Kalman_Set_Par.SigmaInVel[2] = 1E-5;
	// 	Kalman_Set_Par.SigmaInAtt[0] = 0.1f*ATR; Kalman_Set_Par.SigmaInAtt[1] = 0.1f*ATR; Kalman_Set_Par.SigmaInAtt[2] = 0.1f*ATR;

#else

	Kalman_Set_Par.BuffData = Command_Par_Info[COMMAND_KALMAN].Buffer;
	Kalman_Set_Par.SigmaAcc[0] = 0.51;
	Kalman_Set_Par.SigmaAcc[1] = 0.51;
	Kalman_Set_Par.SigmaAcc[2] = 0.51;

	Kalman_Set_Par.SigmaGyro[0] = 0.08f * ATR;
	Kalman_Set_Par.SigmaGyro[1] = 0.08f * ATR;
	Kalman_Set_Par.SigmaGyro[2] = 0.08f * ATR;

	Kalman_Set_Par.AccDrNoise[0] = 0.01;
	Kalman_Set_Par.AccDrNoise[1] = 0.01;
	Kalman_Set_Par.AccDrNoise[2] = 0.01;

	Kalman_Set_Par.GyroDrNoise[0] = 0.01f*ATR;
	Kalman_Set_Par.GyroDrNoise[1] = 0.01f*ATR;
	Kalman_Set_Par.GyroDrNoise[2] = 0.01f*ATR;

	Kalman_Set_Par.SigmaPos[PosN] = Kalman_Set_Par.SigmaPos[PosE] = 0.6;
	Kalman_Set_Par.SigmaPos[PosD] = 0.05;
	Kalman_Set_Par.SigmaVel[VelN] = Kalman_Set_Par.SigmaVel[VelE] = Kalman_Set_Par.SigmaVel[VelD] = 0.05;
	Kalman_Set_Par.SigmaAtt[Roll] = Kalman_Set_Par.SigmaAtt[Pitch] = Kalman_Set_Par.SigmaAtt[Yaw] = 0.1;

	Kalman_Set_Par.SigmaAccBias[AccX] = Kalman_Set_Par.SigmaAccBias[AccY] = Kalman_Set_Par.SigmaAccBias[AccZ] = 0.008;
	Kalman_Set_Par.SigmaGyroBias[GyroX] = Kalman_Set_Par.SigmaGyroBias[GyroY] = Kalman_Set_Par.SigmaGyroBias[GyroZ] = 0.5;

	Kalman_Set_Par.SigmaInPos[0] = 1E-4;
	Kalman_Set_Par.SigmaInPos[1] = 1E-4;
	Kalman_Set_Par.SigmaInPos[2] = 1E-4;

	Kalman_Set_Par.SigmaInVel[0] = 1E-4;
	Kalman_Set_Par.SigmaInVel[1] = 1E-4;
	Kalman_Set_Par.SigmaInVel[2] = 1E-4;

	Kalman_Set_Par.SigmaInAtt[0] = 0.01f*ATR;
	Kalman_Set_Par.SigmaInAtt[1] = 0.01f*ATR;
	Kalman_Set_Par.SigmaInAtt[2] = 0.01f*ATR;

	Kalman_Set_Par.SigmaInAccBias[0] = 0.03;
	Kalman_Set_Par.SigmaInAccBias[1] = 0.03;
	Kalman_Set_Par.SigmaInAccBias[2] = 0.03;

	Kalman_Set_Par.SigmaInGyroBias[0] = 0.2f*ATR;
	Kalman_Set_Par.SigmaInGyroBias[1] = 0.2f*ATR;
	Kalman_Set_Par.SigmaInGyroBias[2] = 0.2f*ATR;

#endif

	Mat_Cal_Info.EG(Kalman_Par.Id, DIM_STATE);
	//H
	Mat_Cal_Info.EG(Kalman_Par.H, DIM_OBS);
	//P the first row
	Mat_Cal_Info.EG(Kalman_Par.P, DIM_STATE);
	//initial the position
	Kalman_Par.P[0] = Kalman_Set_Par.SigmaInPos[PosN] * Kalman_Set_Par.SigmaInPos[PosN];
	Kalman_Par.P[16] = Kalman_Set_Par.SigmaInPos[PosE] * Kalman_Set_Par.SigmaInPos[PosE];
	Kalman_Par.P[32] = Kalman_Set_Par.SigmaInPos[PosD] * Kalman_Set_Par.SigmaInPos[PosD];
	//initial the velocity
	Kalman_Par.P[48] = Kalman_Set_Par.SigmaInVel[VelN] * Kalman_Set_Par.SigmaInVel[VelN];
	Kalman_Par.P[64] = Kalman_Set_Par.SigmaInVel[VelE] * Kalman_Set_Par.SigmaInVel[VelE];
	Kalman_Par.P[80] = Kalman_Set_Par.SigmaInVel[VelD] * Kalman_Set_Par.SigmaInVel[VelD];
	//initial the velocity
	Kalman_Par.P[96] = Kalman_Set_Par.SigmaInAtt[Roll] * Kalman_Set_Par.SigmaInAtt[Roll];
	Kalman_Par.P[112] = Kalman_Set_Par.SigmaInAtt[Pitch] * Kalman_Set_Par.SigmaInAtt[Pitch];
	Kalman_Par.P[128] = Kalman_Set_Par.SigmaInAtt[Yaw] * Kalman_Set_Par.SigmaInAtt[Yaw];
	//intial the acc bias
	Kalman_Par.P[144] = Kalman_Set_Par.SigmaInAccBias[AxisX] * Kalman_Set_Par.SigmaInAccBias[AxisX];
	Kalman_Par.P[160] = Kalman_Set_Par.SigmaInAccBias[AxisY] * Kalman_Set_Par.SigmaInAccBias[AxisY];
	Kalman_Par.P[176] = Kalman_Set_Par.SigmaInAccBias[AxisZ] * Kalman_Set_Par.SigmaInAccBias[AxisZ];
	//intial the gyro bias
	Kalman_Par.P[192] = Kalman_Set_Par.SigmaInGyroBias[AxisX] * Kalman_Set_Par.SigmaInGyroBias[AxisX];
	Kalman_Par.P[208] = Kalman_Set_Par.SigmaInGyroBias[AxisY] * Kalman_Set_Par.SigmaInGyroBias[AxisY];
	Kalman_Par.P[224] = Kalman_Set_Par.SigmaInGyroBias[AxisZ] * Kalman_Set_Par.SigmaInGyroBias[AxisZ];
	//Q
	Mat_Cal_Info.EG(Kalman_Par.Q, DIM_Proc);
	Kalman_Par.Q[0] = Kalman_Set_Par.SigmaAcc[AxisX] * Kalman_Set_Par.SigmaAcc[AxisX];
	Kalman_Par.Q[13] = Kalman_Set_Par.SigmaAcc[AxisY] * Kalman_Set_Par.SigmaAcc[AxisY];
	Kalman_Par.Q[26] = Kalman_Set_Par.SigmaAcc[AxisZ] * Kalman_Set_Par.SigmaAcc[AxisZ];

	Kalman_Par.Q[39] = Kalman_Set_Par.SigmaGyro[AxisX] * Kalman_Set_Par.SigmaGyro[AxisX];
	Kalman_Par.Q[52] = Kalman_Set_Par.SigmaGyro[AxisY] * Kalman_Set_Par.SigmaGyro[AxisY];
	Kalman_Par.Q[65] = Kalman_Set_Par.SigmaGyro[AxisZ] * Kalman_Set_Par.SigmaGyro[AxisZ];

	Kalman_Par.Q[78] = Kalman_Set_Par.AccDrNoise[AxisX] * Kalman_Set_Par.AccDrNoise[AxisX];
	Kalman_Par.Q[91] = Kalman_Set_Par.AccDrNoise[AxisY] * Kalman_Set_Par.AccDrNoise[AxisY];
	Kalman_Par.Q[104] = Kalman_Set_Par.AccDrNoise[AxisZ] * Kalman_Set_Par.AccDrNoise[AxisZ];

	Kalman_Par.Q[117] = Kalman_Set_Par.GyroDrNoise[AxisX] * Kalman_Set_Par.GyroDrNoise[AxisX];
	Kalman_Par.Q[130] = Kalman_Set_Par.GyroDrNoise[AxisY] * Kalman_Set_Par.GyroDrNoise[AxisY];
	Kalman_Par.Q[143] = Kalman_Set_Par.GyroDrNoise[AxisZ] * Kalman_Set_Par.GyroDrNoise[AxisZ];
	//R
	Mat_Cal_Info.EG(Kalman_Par.R, DIM_OBS);
	Kalman_Par.R[0] = Kalman_Set_Par.SigmaPos[PosN] * Kalman_Set_Par.SigmaPos[PosN];
	Kalman_Par.R[16] = Kalman_Set_Par.SigmaPos[PosE] * Kalman_Set_Par.SigmaPos[PosE];
	Kalman_Par.R[32] = Kalman_Set_Par.SigmaPos[PosD] * Kalman_Set_Par.SigmaPos[PosD];

	Kalman_Par.R[48] = Kalman_Set_Par.SigmaVel[VelN] * Kalman_Set_Par.SigmaVel[VelN];
	Kalman_Par.R[64] = Kalman_Set_Par.SigmaVel[VelE] * Kalman_Set_Par.SigmaVel[VelE];
	Kalman_Par.R[80] = Kalman_Set_Par.SigmaVel[VelD] * Kalman_Set_Par.SigmaVel[VelD];

	Kalman_Par.R[96] = Kalman_Set_Par.SigmaAtt[Roll] * Kalman_Set_Par.SigmaAtt[Roll];
	Kalman_Par.R[112] = Kalman_Set_Par.SigmaAtt[Pitch] * Kalman_Set_Par.SigmaAtt[Pitch];
	Kalman_Par.R[128] = Kalman_Set_Par.SigmaAtt[Yaw] * Kalman_Set_Par.SigmaAtt[Yaw];

	Kalman_Par.R[144] = Kalman_Set_Par.SigmaAccBias[AxisX] * Kalman_Set_Par.SigmaAccBias[AxisX];
	Kalman_Par.R[160] = Kalman_Set_Par.SigmaAccBias[AxisY] * Kalman_Set_Par.SigmaAccBias[AxisY];
	Kalman_Par.R[176] = Kalman_Set_Par.SigmaAccBias[AxisZ] * Kalman_Set_Par.SigmaAccBias[AxisZ];

	Kalman_Par.R[192] = Kalman_Set_Par.SigmaGyroBias[AxisX] * Kalman_Set_Par.SigmaGyroBias[AxisX];
	Kalman_Par.R[208] = Kalman_Set_Par.SigmaGyroBias[AxisY] * Kalman_Set_Par.SigmaGyroBias[AxisY];
	Kalman_Par.R[224] = Kalman_Set_Par.SigmaGyroBias[AxisZ] * Kalman_Set_Par.SigmaGyroBias[AxisZ];
}

void KFFilter_Cal_State(vec3 GyroData,vec3 AccData){
	//INS_Nav_Par.ReadTime();
//#if (DIM_STATE == 9)

	float32_t dt = INS_Nav_Par.ReadTime();
	Mat_Cal_Info.MulM(INS_Nav_Par.Cbn, AccData, 3, 3, 1, ft);
	//the first row
	Fc[3] = Fc[19] =  Fc[35] = 1; 

	Fc[51] = 0;      Fc[52] = -ft[2]; Fc[53] = ft[1];  Fc[54] = INS_Nav_Par.Cbn[0]; Fc[55] = INS_Nav_Par.Cbn[1]; Fc[56] = INS_Nav_Par.Cbn[2]; 
	Fc[66] = ft[2];  Fc[67] = 0;      Fc[68] = -ft[0]; Fc[69] = INS_Nav_Par.Cbn[3]; Fc[70] = INS_Nav_Par.Cbn[4]; Fc[71] = INS_Nav_Par.Cbn[5]; 
	Fc[81] = -ft[1]; Fc[82] = ft[0];  Fc[83] = 0;      Fc[84] = INS_Nav_Par.Cbn[6]; Fc[85] = INS_Nav_Par.Cbn[7]; Fc[86] = INS_Nav_Par.Cbn[8]; 

	Fc[102] = -INS_Nav_Par.Cbn[0]; Fc[103] = -INS_Nav_Par.Cbn[1]; Fc[104] = -INS_Nav_Par.Cbn[2];
	Fc[117] = -INS_Nav_Par.Cbn[3]; Fc[118] = -INS_Nav_Par.Cbn[4]; Fc[119] = -INS_Nav_Par.Cbn[5];
    Fc[132] = -INS_Nav_Par.Cbn[6]; Fc[133] = -INS_Nav_Par.Cbn[7]; Fc[134] = -INS_Nav_Par.Cbn[8];

	Fc[144] = Fc[160] = Fc[176] = -1e-11;   //gyro bias time constant
	Fc[192] = Fc[208] = Fc[224] = -1e-11;   //acc bias time constant


	Gc[36] = INS_Nav_Par.Cbn[0]; Gc[37] = INS_Nav_Par.Cbn[1]; Gc[38] = INS_Nav_Par.Cbn[2]; 
	Gc[48] = INS_Nav_Par.Cbn[3]; Gc[49] = INS_Nav_Par.Cbn[4]; Gc[50] = INS_Nav_Par.Cbn[5]; 
	Gc[60] = INS_Nav_Par.Cbn[6]; Gc[61] = INS_Nav_Par.Cbn[7]; Gc[62] = INS_Nav_Par.Cbn[8]; 

	Gc[75] = -INS_Nav_Par.Cbn[0]; Gc[76] = -INS_Nav_Par.Cbn[1]; Gc[77] = -INS_Nav_Par.Cbn[2];
	Gc[87] = -INS_Nav_Par.Cbn[3]; Gc[88] = -INS_Nav_Par.Cbn[4]; Gc[89] = -INS_Nav_Par.Cbn[5];
	Gc[99] = -INS_Nav_Par.Cbn[6]; Gc[100] = -INS_Nav_Par.Cbn[7]; Gc[101] = -INS_Nav_Par.Cbn[8];

	Gc[114] = Gc[127] = Gc[140] = Gc[153] = Gc[166] = Gc[179] = 1;
//#endif
	Mat_Cal_Info.MulD(Fc, DIM_STATE, DIM_STATE, INS_Cal_Par.dt[0], tempDSxDS);
	Mat_Cal_Info.Add(tempDSxDS,Kalman_Par.Id, DIM_STATE, DIM_STATE, Kalman_Par.F);
	Mat_Cal_Info.MulD(Gc, DIM_STATE, DIM_Proc, INS_Cal_Par.dt[0], Kalman_Par.G);

}
void KFFilter_State_Update(void){
	uint8_t i = 0, window = 0;
//	float32_t tempdata = 0;
	/* Calculate the prediction error. Since the detector hypothesis
	 is that the platform has zero velocity, the prediction error is
	 equal to zero minu the estimated velocity.  */
	window = 3;// PDR_Set_Par.Buffer.BufferSize - 1;
	if (PDR_Set_Par.Buffer.ZuptBuff[window] == true){
		Kalman_Par.Z[0] = 0 - (INS_Nav_Par.Data.Pos[window][PosN] - INS_Nav_Par.Pos[PosN]);
		Kalman_Par.Z[1] = 0 - (INS_Nav_Par.Data.Pos[window][PosE] - INS_Nav_Par.Pos[PosE]);
		Kalman_Par.Z[2] = 0 - (INS_Nav_Par.Data.Pos[window][PosD] - INS_Nav_Par.Pos[PosD]);

		Kalman_Par.Z[6] = 0 - (INS_Nav_Par.Data.Att[window][Roll] - INS_Nav_Par.Att[Roll]);
		Kalman_Par.Z[7] = 0 - (INS_Nav_Par.Data.Att[window][Pitch] - INS_Nav_Par.Att[Pitch]);
		Kalman_Par.Z[8] = 0 - (INS_Nav_Par.Data.Att[window][Yaw] - INS_Nav_Par.Att[Yaw]);
	}
	else{
		Kalman_Par.Z[0] = Kalman_Par.Z[1] = Kalman_Par.Z[2] = 0;
		Kalman_Par.Z[6] = Kalman_Par.Z[7] = Kalman_Par.Z[8] = 0;
	}


	Kalman_Par.Z[3] = 0 - INS_Nav_Par.Vel[VelN];     //oberserve refresh
	Kalman_Par.Z[4] = 0 - INS_Nav_Par.Vel[VelE];
	Kalman_Par.Z[5] = 0 - INS_Nav_Par.Vel[VelD];

	Kalman_Par.Z[9]  =   PDR_Set_Par.ZeroData.Mean[Acc][AccX] - Dev_Sol_Info[DEV_AX].Out_Data ;
	Kalman_Par.Z[10] =   PDR_Set_Par.ZeroData.Mean[Acc][AccY] - Dev_Sol_Info[DEV_AY].Out_Data;
	Kalman_Par.Z[11] =   PDR_Set_Par.ZeroData.Mean[Acc][AccZ] - Dev_Sol_Info[DEV_AZ].Out_Data;

	Kalman_Par.Z[12] =   PDR_Set_Par.ZeroData.Mean[Gyro][GyroX] - Dev_Sol_Info[DEV_GX].Out_Data*ATR;
	Kalman_Par.Z[13] =   PDR_Set_Par.ZeroData.Mean[Gyro][GyroY] - Dev_Sol_Info[DEV_GY].Out_Data*ATR;
	Kalman_Par.Z[14] =   PDR_Set_Par.ZeroData.Mean[Gyro][GyroZ] - Dev_Sol_Info[DEV_GZ].Out_Data*ATR;

	/*Estimation of the perturbations in the estimated navigation states   dx=K*z*/
	Mat_Cal_Info.MulM(Kalman_Par.K, Kalman_Par.Z, DIM_STATE, DIM_OBS, 1, Kalman_Par.dX);

	/*Correct the navigation state using the estimated perturbations. */
	INS_Nav_Par.Pos[PosN] = INS_Nav_Par.Pos[PosN] + Kalman_Par.dX[0];
	INS_Nav_Par.Pos[PosE] = INS_Nav_Par.Pos[PosE] + Kalman_Par.dX[1];
	INS_Nav_Par.Pos[PosD] = INS_Nav_Par.Pos[PosD] + Kalman_Par.dX[2];

// 	tempdata=INS_Nav_Par.Data.Pos[0][PosD] - INS_Nav_Par.Data.Pos[9][PosD];
// 	if (fabs(tempdata) < 20){
// 		INS_Nav_Par.Pos[PosD] = INS_Nav_Par.Data.Pos[0][PosD] = 0;
// 	}

	INS_Nav_Par.Vel[VelN] = INS_Nav_Par.Vel[VelN] + Kalman_Par.dX[3];
	INS_Nav_Par.Vel[VelE] = INS_Nav_Par.Vel[VelE] + Kalman_Par.dX[4];
	INS_Nav_Par.Vel[VelD] = INS_Nav_Par.Vel[VelD] + Kalman_Par.dX[5];

	INS_Sensor_Par[Acc].Bias[AxisX] = Kalman_Par.dX[9];
	INS_Sensor_Par[Acc].Bias[AxisY] = Kalman_Par.dX[10];
	INS_Sensor_Par[Acc].Bias[AxisZ] = Kalman_Par.dX[11];

	INS_Sensor_Par[Gyro].Bias[AxisX] = Kalman_Par.dX[12];
	INS_Sensor_Par[Gyro].Bias[AxisY] = Kalman_Par.dX[13];
	INS_Sensor_Par[Gyro].Bias[AxisZ] = Kalman_Par.dX[14];

	/*Correct the rotation matrics*/
	dcmError[0] = 0;                 dcmError[1] = -Kalman_Par.dX[8]; dcmError[2] = Kalman_Par.dX[7];
	dcmError[3] =  Kalman_Par.dX[8]; dcmError[4] = 0;                 dcmError[5] = -Kalman_Par.dX[6];
	dcmError[6] = -Kalman_Par.dX[7]; dcmError[7] = Kalman_Par.dX[6];  dcmError[8] = 0;

	/*Convert quaternion to a rotation matrix*/
	Mat_Cal_Info.Sub(Id, dcmError, 3, 3, M3temp1);
	Mat_Cal_Info.MulM(M3temp1, INS_Nav_Par.Cbn, 3, 3, 3, M3temp2);
	for (i = 0; i < 9;i++){
		INS_Nav_Par.Cbn[i] = M3temp2[i];
	}
	INS_Nav_Par.dcm2e(INS_Nav_Par.Cbn, INS_Nav_Par.Att);
	INS_Nav_Par.dcm2q(INS_Nav_Par.Cbn, INS_Nav_Par.Q);

}

