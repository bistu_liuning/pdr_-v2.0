/**/
#ifndef __PDRSOLVE_H
#define __PDRSOLVE_H

#include "PlatformCon.h"
#include "INSSolve.h"
#include "Matrix.h"
#include "KFFilter.h"


typedef struct
{
	uint16_t BufferSize;  //cbn q att vel pos the five parameters
	float32_t GyroBias[100][3];
	float32_t AccBias[100][3];
	float32_t Mean[2][3];
	float32_t Sum[2][3];
}PDR_Zero_Buff_Info;

typedef struct 
{
	uint16_t BufferSize;
	float32_t ZuptBuff[100];
}PDR_Buff_Info;

typedef struct{
  uint8_t Mode;
  uint8_t Zupt;
  uint8_t Zaru;
  uint32_t StepNum;

  float32_t Sigma_a;
  float32_t Sigma_g;
  float32_t Gamma;
  float32_t WindowSize;
  float32_t *BuffData;
    
  float32_t Tk;

  float32_t HDRTre;
  float32_t HDRWindow;
  float32_t Angle[3];

  float32_t Pos[3];

  PDR_Zero_Buff_Info ZeroData;
  PDR_Buff_Info Buffer;
}PDR_Info;

typedef struct{
	void(*Init) (void);
	void(*Detector)(void);
	void(*Solve) (void);
	void(*InitBuffer) (void);
}PDR_Func_Info;

extern PDR_Info PDR_Set_Par;
extern PDR_Func_Info PDR_Cal_Func;

void PDR_Detector(void);
void PDRSolve_Init(void);
void PDR_Cal(void);
void PDR_Zero_Buffer(void);
#endif



