/**/
#include "PDRsolve.h"

void PDRSolve_Init(void){
#if defined(VS_SIM)

#else
  PDR_Set_Par.BuffData = Command_Par_Info[COMMAND_USER].Buffer;
  PDR_Set_Par.Mode     = (uint8_t) PDR_Set_Par.BuffData[0];
  PDR_Set_Par.Sigma_a  = PDR_Set_Par.BuffData[1];
  PDR_Set_Par.Sigma_g  = PDR_Set_Par.BuffData[2];
  PDR_Set_Par.WindowSize = PDR_Set_Par.BuffData[3]; 
  PDR_Set_Par.Gamma    =  PDR_Set_Par.BuffData[4];
  INS_Cal_Par.BufferSize = PDR_Set_Par.WindowSize;
#endif
  PDR_Set_Par.ZeroData.BufferSize = 50;
  PDR_Set_Par.Buffer.BufferSize = 10;     //To be design, waiting for test
}

void PDR_Cal(void){

#if defined(VS_SIM)
	PDR_Set_Par.Mode     = 0;
	PDR_Set_Par.Sigma_a  = 5.5;
	PDR_Set_Par.Sigma_g  =  0.0105;
	PDR_Set_Par.WindowSize = 10; 
	PDR_Set_Par.Gamma    =  2000;
	PDR_Set_Par.HDRTre = 0.15;
	PDR_Set_Par.HDRWindow =3;
	INS_Cal_Par.BufferSize = PDR_Set_Par.WindowSize;
	
#else
	PDR_Set_Par.Mode     = 0;
	PDR_Set_Par.Sigma_a  = 5.5;
	PDR_Set_Par.Sigma_g  =  0.0105;
	PDR_Set_Par.WindowSize = 10; 
	PDR_Set_Par.Gamma    =  2000;
	PDR_Set_Par.HDRTre = 0.15;
	PDR_Set_Par.HDRWindow =3;
	INS_Cal_Par.BufferSize = PDR_Set_Par.WindowSize;
#endif 
	Kalman_Cal_Func.StateCal(INS_Sensor_Par[Gyro].data, INS_Sensor_Par[Acc].data);
	Kalman_Cal_Func.TimeUpdate();
	PDR_Cal_Func.Detector();
	if (PDR_Set_Par.Zupt == true){
		Kalman_Cal_Func.MeasureUpdate();
		Kalman_Cal_Func.StateUpdate();
	}
	else{
		PDR_Set_Par.Angle[Yaw] = INS_Nav_Par.Att[Yaw];
		PDR_Set_Par.Angle[Roll] = INS_Nav_Par.Att[Roll];
		PDR_Set_Par.Angle[Pitch] = INS_Nav_Par.Att[Pitch];
	}

}

void PDR_Zero_Buffer(void){
	uint16_t i = 0, j = 0;

	for (j = 0; j < 3; j++)
	{
		PDR_Set_Par.ZeroData.Sum[Gyro][j] = 0;
		PDR_Set_Par.ZeroData.Sum[Acc][j] = 0;
		for (i = 0; i < PDR_Set_Par.ZeroData.BufferSize - 1; i++)
		{
			PDR_Set_Par.ZeroData.GyroBias[PDR_Set_Par.ZeroData.BufferSize - 1 - i][j] =
				PDR_Set_Par.ZeroData.GyroBias[PDR_Set_Par.ZeroData.BufferSize - 1 - i - 1][j];
			PDR_Set_Par.ZeroData.Sum[Gyro][j] = PDR_Set_Par.ZeroData.Sum[Gyro][j] +
				PDR_Set_Par.ZeroData.GyroBias[PDR_Set_Par.ZeroData.BufferSize - 1 - i][j];
			
			PDR_Set_Par.ZeroData.AccBias[PDR_Set_Par.ZeroData.BufferSize - 1 - i][j] =
				PDR_Set_Par.ZeroData.AccBias[PDR_Set_Par.ZeroData.BufferSize - 1 - i - 1][j];
			PDR_Set_Par.ZeroData.Sum[Acc][j] = PDR_Set_Par.ZeroData.Sum[Acc][j] +
				PDR_Set_Par.ZeroData.AccBias[PDR_Set_Par.ZeroData.BufferSize - 1 - i][j];
		}
		PDR_Set_Par.ZeroData.GyroBias[0][j] = INS_Sensor_Par[Gyro].data[j];
		PDR_Set_Par.ZeroData.Mean[Gyro][j] = PDR_Set_Par.ZeroData.Sum[Gyro][j] / PDR_Set_Par.ZeroData.BufferSize;
		PDR_Set_Par.ZeroData.AccBias[0][j] = INS_Sensor_Par[Acc].data[j];
		PDR_Set_Par.ZeroData.Mean[Acc][j] = PDR_Set_Par.ZeroData.Sum[Acc][j] / PDR_Set_Par.ZeroData.BufferSize;
	}
}

