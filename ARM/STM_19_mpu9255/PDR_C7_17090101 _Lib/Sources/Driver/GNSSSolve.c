/***************************************************************** 
*  @brief:         GNSS signal Solve about BMD njust GPS and UBLOX 
*  @File:          
*  @Project:     
*  @Author:      
*  @Date:         
*  @CopyRight:
*  @Version:     
*  @Description:
*****************************************************************/
#include "GNSSSolve.h"


/***************************************************************** 
*  @brief:          
*  @Function:   
*  @Author:       
*  @Date:         
*  @CopyRight:
*  @Version:     
*  @Description:
*****************************************************************/
void GPS_MBT_Resolve(uint8_t *buf,uint16_t num)
{
	uint16_t i=0;
	float64_t Temp64Buf[5]={0};
	float32_t Temp32Buf[7]={0};
	uint8_t *Send_Q,*Send_P;
  uint8_t *Send_Q1,*Send_P1;
	//Message Solve
	GNSSInfo.DaySecond  =(uint32_t)(buf[6] |(buf[7]<<8) |(buf[8]<<16) |(buf[9]<<24));
	GNSSInfo.WeekSecond =(uint32_t)(buf[10]|(buf[11]<<8)|(buf[12]<<16)|(buf[13]<<24));
	GNSSInfo.NavType=(uint32_t)(buf[14]|(buf[15]<<8)|(buf[16]<<16)|(buf[17]<<24));
	
	Send_Q1=(uint8_t *)&Temp64Buf;	 
	Send_P1=&buf[18];               
	for(i=0;i<5*8;i++)                                //�?51?D��??2?�????�?a?3D�
	{							
		*Send_Q1=*Send_P1;
		Send_Q1++;
		Send_P1++;
	}
	GNSSInfo.DesX=Temp64Buf[0];
	GNSSInfo.DesY=Temp64Buf[1];
	GNSSInfo.DesZ=Temp64Buf[2];
	GNSSInfo.Lat=Temp64Buf[3];
	GNSSInfo.Lon=Temp64Buf[4];
	
	Send_Q=(uint8_t *)&Temp32Buf;	              //754 ��?�?�?????�o�
	Send_P=&buf[58];               
	for(i=0;i<7*4;i++)                    
	{							
		*Send_Q=*Send_P;
		Send_Q++;
		Send_P++;
	}
	GNSSInfo.high=Temp32Buf[0];
	GNSSInfo.Vx=Temp32Buf[1];
	GNSSInfo.Vy=Temp32Buf[2];
	GNSSInfo.Vz=Temp32Buf[3];
	GNSSInfo.Ve=Temp32Buf[4];
	GNSSInfo.Vn=Temp32Buf[5];
	GNSSInfo.Vu=Temp32Buf[6];
	
	GNSSInfo.SatNum=buf[86];
	GNSSInfo.PDOP=buf[87];
	
	GNSSInfo.FirTime=(uint32_t)(buf[88]|(buf[89]>>8)|(buf[90]>>16)|(buf[91]>>24));
}
/***************************************************************** 
*  @brief:         
*  @File:          
*  @Project:     
*  @Author:      
*  @Date:         
*  @CopyRight:
*  @Version:     
*  @Description:
*****************************************************************/
void GPS_0183_Resolve(uint8_t *buf,uint16_t data_num)
{
	uint8_t comma=0,num_1=1;          //Record point
	uint8_t n;
	uint8_t Comma_Num[50]={0};  //�洢����λ��
	uint8_t Point_Num[50]={0};
	float32_t Latitude_Degree=0,Latitude_Cent=0,Latitude_Second=0;
	float32_t Longitude_Degree=0,Longitude_Cent=0,Longitude_Second=0;
	float32_t Temp_High=0,Temp_A_MCT=0,Temp_V_MCN=0,Temp_Angle_T=0,Temp_V_K=0,Temp_Angle_M=0,Temp_V_N=0;
	float32_t La=0,Lo=0;
	float32_t IG_V_N=0,IG_V_K=0;
  uint8_t buf_GGA[80]={0};
  uint8_t buf_VTG[80]={0};
  uint16_t num=0;
	
	if((buf[4]=='G') && (buf[5]=='A'))     //GPGGA�ӵ���֮ǰ ֮����56  67���ַ�
	{
		for(n=0;n<data_num-2;n++) 
		{
			buf_GGA[n]=buf[n]; 
		}
	}
	
	if((buf[4]=='T') && (buf[5]=='G'))     //GPVTG
	{
		for(n=0;n<data_num-2;n++) 
		{
			buf_VTG[n]=buf[n];
		}
	}                                             

	for(n=0;n<data_num-2;n++)   //�ж϶���
	{
	     if(buf[n]==',')  
	     {
	        Comma_Num[comma]=n;
	        comma++;
	     }
	     if(buf[n]=='.')
	     {
	        Point_Num[comma-1]=n;
	     }
	}
	if((buf[4]=='G') && (buf[5]=='A'))
	{
     GNSSAdvInfo.UTCTime=(buf[Comma_Num[0]+1]-0x30)*100000+(buf[Comma_Num[0]+2]-0x30)*10000+(buf[Comma_Num[0]+3]-0x30)*1000
	           +(buf[Comma_Num[0]+4]-0x30)*100+(buf[Comma_Num[0]+5]-0x30)*10+(buf[Comma_Num[0]+6]-0x30)
			   +(buf[Comma_Num[0]+8]-0x30)*0.1+(buf[Comma_Num[0]+9]-0x30)*0.01+(buf[Comma_Num[0]+10]-0x30)*0.001;	   

	   Latitude_Degree=((float64_t)(buf[Comma_Num[1]+1]-0x30))*10+((float64_t)(buf[Comma_Num[1]+2]-0x30));
	   Latitude_Cent=((float64_t)(buf[Comma_Num[1]+3]-0x30))*10+((float64_t)(buf[Comma_Num[1]+4]-0x30));
	   Latitude_Second=((float64_t)(buf[Comma_Num[1]+6]-0x30))*0.1+((float64_t)(buf[Comma_Num[1]+7]-0x30))*0.01+((float64_t)(buf[Comma_Num[1]+8]-0x30))*0.001+((float64_t)(buf[Comma_Num[1]+9]-0x30))*0.0001;
	   GNSSAdvInfo.Lat=(Latitude_Degree+(Latitude_Cent+Latitude_Second)/60);
	   Longitude_Degree=((float64_t)(buf[Comma_Num[3]+1]-0x30))*100+((float64_t)(buf[Comma_Num[3]+2]-0x30))*10+((float64_t)(buf[Comma_Num[3]+3]-0x30));
	   Longitude_Cent=((float64_t)(buf[Comma_Num[3]+4]-0x30))*10+((float64_t)(buf[Comma_Num[3]+5]-0x30));
	   Longitude_Second=((float64_t)(buf[Comma_Num[3]+7]-0x30))*0.1+((float64_t)(buf[Comma_Num[3]+8]-0x30))*0.01+((float64_t)(buf[Comma_Num[3]+9]-0x30))*0.001+((float64_t)(buf[Comma_Num[3]+10]-0x30))*0.0001;
	    GNSSAdvInfo.Lon=(Longitude_Degree+(Longitude_Cent+Longitude_Second)/60);
	   //����߶�   δ������ֵ����
	   if((Comma_Num[9]-1)>Comma_Num[8])   //�жϸ߶��Ƿ�Ϊ�գ�����߶���Ϣ��ȡ
	   {
		   num=Point_Num[8]-Comma_Num[8]-1;  //��ȡС����ǰ������
		   num_1=1;
		   while(num_1<(Point_Num[8]-Comma_Num[8]-1))
		   {
		      Temp_High=Temp_High+((float64_t)(buf[Comma_Num[8]+num_1]-0x30))*pow(10,(num-1));
		      num_1++;
		      num--;
		   }
		   Temp_High=Temp_High+((float64_t)(buf[Comma_Num[8]+num_1]-0x30));	   
		   num=Comma_Num[9]-Point_Num[8]-1;   //��ȡС����������
		   num_1=1;
		   while(num_1<(Comma_Num[9]-Point_Num[8]-1))
		   {
		      Temp_High=Temp_High+((float64_t)(buf[Comma_Num[9]-num_1]-0x30))*pow(0.1,(num-1));
		      num_1++;
		      num--;
		   }
		   Temp_High=Temp_High+((float64_t)(buf[Point_Num[8]+num_1]-0x30))*0.1;
		   GNSSAdvInfo.high=Temp_High;
	   }	   
	   GNSSAdvInfo.Lo_EW=buf[Comma_Num[4]+1];
	   GNSSAdvInfo.La_SN=buf[Comma_Num[2]+1];
	   GNSSAdvInfo.NavType=buf[Comma_Num[5]+1]-0x30;  //��ȡ���Ǳ�־λ 0-δ��λ 1-���ֶ�λ 2-��ֶ�λ 3-��ЧGPS 6-���ڹ���
	   GNSSAdvInfo.SatNum=(buf[Comma_Num[6]+1]-0x30)*10+(buf[Comma_Num[6]+2]-0x30);   //��ȡ����������
	   buf_GGA[Comma_Num[1]+1]=La/10+0x30;
	   buf_GGA[Comma_Num[1]+2]=(int16_t)La%10+0x30;
	   buf_GGA[Comma_Num[1]+3]=((La-(buf_GGA[Comma_Num[1]+1]-0x30)*10-(buf_GGA[Comma_Num[1]+2]-0x30))*60)/10+0x30;
	   buf_GGA[Comma_Num[1]+4]=(int16_t)((La-(buf_GGA[Comma_Num[1]+1]-0x30)*10-(buf_GGA[Comma_Num[1]+2]-0x30))*60)%10+0x30;
	   buf_GGA[Comma_Num[1]+5]='.';
	   buf_GGA[Comma_Num[1]+6]=((La-(buf_GGA[Comma_Num[1]+1]-0x30)*10-(buf_GGA[Comma_Num[1]+2]-0x30))*60-(buf_GGA[Comma_Num[1]+3]-0x30)*10-(buf_GGA[Comma_Num[1]+4]-0x30))*10+0x30;
	   buf_GGA[Comma_Num[1]+7]=(((La-(buf_GGA[Comma_Num[1]+1]-0x30)*10-(buf_GGA[Comma_Num[1]+2]-0x30))*60-(buf_GGA[Comma_Num[1]+3]-0x30)*10-(buf_GGA[Comma_Num[1]+4]-0x30))*10-(buf_GGA[Comma_Num[1]+6]-0x30))*10+0x30;
	   buf_GGA[Comma_Num[1]+8]=((((La-(buf_GGA[Comma_Num[1]+1]-0x30)*10-(buf_GGA[Comma_Num[1]+2]-0x30))*60-(buf_GGA[Comma_Num[1]+3]-0x30)*10-(buf_GGA[Comma_Num[1]+4]-0x30))*10-(buf_GGA[Comma_Num[1]+6]-0x30))*10-(buf_GGA[Comma_Num[1]+7]-0x30))*10+0x30;
	   buf_GGA[Comma_Num[1]+9]=(((((La-(buf_GGA[Comma_Num[1]+1]-0x30)*10-(buf_GGA[Comma_Num[1]+2]-0x30))*60-(buf_GGA[Comma_Num[1]+3]-0x30)*10-(buf_GGA[Comma_Num[1]+4]-0x30))*10-(buf_GGA[Comma_Num[1]+6]-0x30))*10-(buf_GGA[Comma_Num[1]+7]-0x30))*10-(buf_GGA[Comma_Num[1]+8]-0x30))*10+0x30;
	   
	   buf_GGA[Comma_Num[3]+1]=Lo/100+0x30;
	   buf_GGA[Comma_Num[3]+2]=(Lo-(buf_GGA[Comma_Num[3]+1]-0x30)*100)/10+0x30;
	   buf_GGA[Comma_Num[3]+3]=(int16_t)(Lo-(buf_GGA[Comma_Num[3]+1]-0x30)*100)%10+0x30;
	   buf_GGA[Comma_Num[3]+4]=(Lo-(buf_GGA[Comma_Num[3]+1]-0x30)*100-(buf_GGA[Comma_Num[3]+2]-0x30)*10-(buf_GGA[Comma_Num[3]+3]-0x30))*60/10+0x30;
	   buf_GGA[Comma_Num[3]+5]=(int16_t)((Lo-(buf_GGA[Comma_Num[3]+1]-0x30)*100-(buf_GGA[Comma_Num[3]+2]-0x30)*10-(buf_GGA[Comma_Num[3]+3]-0x30))*60)%10+0x30;
	   buf_GGA[Comma_Num[3]+6]='.';
	   buf_GGA[Comma_Num[3]+7]=((Lo-(buf_GGA[Comma_Num[3]+1]-0x30)*100-(buf_GGA[Comma_Num[3]+2]-0x30)*10-(buf_GGA[Comma_Num[3]+3]-0x30))*60-(buf_GGA[Comma_Num[3]+4]-0x30)*10-(buf_GGA[Comma_Num[3]+5]-0x30))*10+0x30;
	   buf_GGA[Comma_Num[3]+8]=(((Lo-(buf_GGA[Comma_Num[3]+1]-0x30)*100-(buf_GGA[Comma_Num[3]+2]-0x30)*10-(buf_GGA[Comma_Num[3]+3]-0x30))*60-(buf_GGA[Comma_Num[3]+4]-0x30)*10-(buf_GGA[Comma_Num[3]+5]-0x30))*10-(buf_GGA[Comma_Num[3]+7]-0x30))*10+0x30;
	   buf_GGA[Comma_Num[3]+9]=((((Lo-(buf_GGA[Comma_Num[3]+1]-0x30)*100-(buf_GGA[Comma_Num[3]+2]-0x30)*10-(buf_GGA[Comma_Num[3]+3]-0x30))*60-(buf_GGA[Comma_Num[3]+4]-0x30)*10-(buf_GGA[Comma_Num[3]+5]-0x30))*10-(buf_GGA[Comma_Num[3]+7]-0x30))*10-(buf_GGA[Comma_Num[3]+8]-0x30))*10+0x30;
	   buf_GGA[Comma_Num[3]+10]=(((((Lo-(buf_GGA[Comma_Num[3]+1]-0x30)*100-(buf_GGA[Comma_Num[3]+2]-0x30)*10-(buf_GGA[Comma_Num[3]+3]-0x30))*60-(buf_GGA[Comma_Num[3]+4]-0x30)*10-(buf_GGA[Comma_Num[3]+5]-0x30))*10-(buf_GGA[Comma_Num[3]+7]-0x30))*10-(buf_GGA[Comma_Num[3]+8]-0x30))*10-(buf_GGA[Comma_Num[3]+9]-0x30))*10+0x30;
	}
	if((buf[4]=='M') && (buf[5]=='C'))  //GPRMC�ֶ�������    GPRMC�ֶεķ�λ�Ǻͽ��ٶȣ�
	{
		if((Comma_Num[8]-1)>Comma_Num[7])  //�ж��ٶ��ֶ��Ƿ�Ϊ��
		{
			Temp_A_MCT=((double)(buf[Comma_Num[7]+1]-0x30))*100+((double)(buf[Comma_Num[7]+2]-0x30))*10+((double)(buf[Comma_Num[7]+3]-0x30))+((double)(buf[Comma_Num[7]+5]-0x30))*0.1;
			GNSSAdvInfo.Angle_MCT=Temp_A_MCT;		     
		}
		if((Comma_Num[7]-1)>Comma_Num[6])  //�ж��ٶ��ֶ��Ƿ�Ϊ��
		{
			Temp_V_MCN=((double)(buf[Comma_Num[6]+1]-0x30))*100+((double)(buf[Comma_Num[6]+2]-0x30))*10+((double)(buf[Comma_Num[6]+3]-0x30))+((double)(buf[Comma_Num[6]+5]-0x30))*0.1;
			Temp_V_MCN=Temp_V_MCN*0.51444f;
			GNSSAdvInfo.V_MCN=Temp_V_MCN;
		}			
	}
	if((buf[4]=='T') && (buf[5]=='G'))  //GPVTG�ֶ�������ȡ���汱�ο�ϵ�˶��Ƕȣ��ű��ο�ϵ�˶��Ƕȣ�ˮƽ�˶��ٶȣ�
	{	
		if((Comma_Num[1]-1)>Comma_Num[0]) //�ж��ֶ����Ƿ�Ϊ�գ���ͬ...
		{
		   //Gps_Angle_T=((double)(buf[Comma_Num[0]+1]-0x30))*100+((double)(buf[Comma_Num[0]+2]-0x30))*10+((double)(buf[Comma_Num[0]+3]-0x30))+((double)(buf[Comma_Num[0]+5]-0x30))*0.1;
		   num=Point_Num[0]-Comma_Num[0]-1;  //��ȡС����ǰ������
		   num_1=1;
		   while(num_1<(Point_Num[0]-Comma_Num[0]-1))
		   {
		      Temp_Angle_T=Temp_Angle_T+((double)(buf[Comma_Num[0]+num_1]-0x30))*pow(10,(num-1));
		      num_1++;
		      num--;
		   }
		   Temp_Angle_T=Temp_Angle_T+((double)(buf[Comma_Num[0]+num_1]-0x30));	   
		   num=Comma_Num[1]-Point_Num[0]-1;   //��ȡС����������
		   num_1=1;
		   while(num_1<(Comma_Num[1]-Point_Num[0]-1))
		   {
		      Temp_Angle_T=Temp_Angle_T+((double)(buf[Comma_Num[1]-num_1]-0x30))*pow(0.1,(num-1));
		      num_1++;
		      num--;
		   }
		   Temp_Angle_T=Temp_Angle_T+((double)(buf[Point_Num[0]+num_1]-0x30))*0.1;
		   GNSSAdvInfo.Angle_T=Temp_Angle_T;
			 	   //GPS�Ƕȸ�ֵ
	     //if(Gps_Angle_T>180)  Gps_Angle_T=Gps_Angle_T-360;
		}
		if((Comma_Num[3]-1)>Comma_Num[2])
		{
		   //Gps_Angle_M=((double)(buf[Comma_Num[2]+1]-0x30))*100+((double)(buf[Comma_Num[2]+2]-0x30))*10+((double)(buf[Comma_Num[2]+3]-0x30))+((double)(buf[Comma_Num[2]+5]-0x30))*0.1;
		   num=Point_Num[2]-Comma_Num[2]-1;  //��ȡС����ǰ������
		   num_1=1;
		   while(num_1<(Point_Num[2]-Comma_Num[2]-1))
		   {
		      Temp_Angle_M=Temp_Angle_M+((float64_t)(buf[Comma_Num[2]+num_1]-0x30))*pow(10,(num-1));
		      num_1++;
		      num--;
		   }
		   Temp_Angle_M=Temp_Angle_M+((float64_t)(buf[Comma_Num[2]+num_1]-0x30));	   
		   num=Comma_Num[3]-Point_Num[2]-1;   //��ȡС����������
		   num_1=1;
		   while(num_1<(Comma_Num[3]-Point_Num[2]-1))
		   {
		      Temp_Angle_M=Temp_Angle_M+((float64_t)(buf[Comma_Num[3]-num_1]-0x30))*pow(0.1,(num-1));
		      num_1++;
		      num--;
		   }
		   Temp_Angle_M=Temp_Angle_M+((float64_t)(buf[Point_Num[2]+num_1]-0x30))*0.1;
		   GNSSAdvInfo.Angle_M=Temp_Angle_M;
		}
		if((Comma_Num[5]-1)>Comma_Num[4])
		{
		   num=Point_Num[4]-Comma_Num[4]-1;  //��ȡС����ǰ������
		   num_1=1;
		   while(num_1<(Point_Num[4]-Comma_Num[4]-1))
		   {
		      Temp_V_N=Temp_V_N+((float64_t)(buf[Comma_Num[4]+num_1]-0x30))*pow(10,(num-1));
		      num_1++;
		      num--;
		   }
		   Temp_V_N=Temp_V_N+((float64_t)(buf[Comma_Num[4]+num_1]-0x30));	   
		   num=Comma_Num[5]-Point_Num[4]-1;   //��ȡС����������
		   num_1=1;
		   while(num_1<(Comma_Num[5]-Point_Num[4]-1))
		   {
		      Temp_V_N=Temp_V_N+((float64_t)(buf[Comma_Num[5]-num_1]-0x30))*pow(0.1,(num-1));
		      num_1++;
		      num--;
		   }
		   Temp_V_N=Temp_V_N+((float64_t)(buf[Point_Num[4]+num_1]-0x30))*0.1;
		   GNSSAdvInfo.V_N=Temp_V_N;
		}
		if((Comma_Num[7]-1)>Comma_Num[6])
		{
//			Gps_V_K=((double)(buf[Comma_Num[6]+1]-0x30))*1000+((double)(buf[Comma_Num[6]+2]-0x30))*100+((double)(buf[Comma_Num[6]+3]-0x30))*10+((double)(buf[Comma_Num[6]+4]-0x30))+((double)(buf[Comma_Num[6]+6]-0x30))*0.1;		
//			Gps_V_K=Gps_V_K;
		   //Gps_Angle_T=((double)(buf[Comma_Num[0]+1]-0x30))*100+((double)(buf[Comma_Num[0]+2]-0x30))*10+((double)(buf[Comma_Num[0]+3]-0x30))+((double)(buf[Comma_Num[0]+5]-0x30))*0.1;
		   num=Point_Num[6]-Comma_Num[6]-1;  //��ȡС����ǰ������
		   num_1=1;
		   while(num_1<(Point_Num[6]-Comma_Num[6]-1))
		   {
		      Temp_V_K=Temp_V_K+((float64_t)(buf[Comma_Num[6]+num_1]-0x30))*pow(10,(num-1));
		      num_1++;
		      num--;
		   }
		   Temp_V_K=Temp_V_K+((float64_t)(buf[Comma_Num[6]+num_1]-0x30));	   
		   num=Comma_Num[7]-Point_Num[6]-1;   //��ȡС����������
		   num_1=1;
		   while(num_1<(Comma_Num[7]-Point_Num[6]-1))
		   {
		      Temp_V_K=Temp_V_K+((float64_t)(buf[Comma_Num[7]-num_1]-0x30))*pow(0.1,(num-1));
		      num_1++;
		      num--;
		   }
		   Temp_V_K=Temp_V_K+((float64_t)(buf[Point_Num[6]+num_1]-0x30))*0.1;
		   GNSSAdvInfo.V_K=Temp_V_K;
		}
		//IG_V_N=(sqrt(data_exp(Vn,2)+data_exp(Ve,2))+Gps_V_N)*0.5/0.51444;
		//IG_V_K=(sqrt(data_exp(Vn,2)+data_exp(Ve,2))+Gps_V_K)*0.5/0.27777;
		GNSSAdvInfo.Vn=(GNSSAdvInfo.V_K/3.6f)*cos((float32_t)GNSSAdvInfo.Angle_T/57.3f);   //GPS �����ٶ�
		GNSSAdvInfo.Ve=(GNSSAdvInfo.V_K/3.6f)*sin((float32_t)GNSSAdvInfo.Angle_T/57.3f);   //GPS �����ٶ�
		buf_VTG[Comma_Num[4]+1]=IG_V_N/100+0x30;
		buf_VTG[Comma_Num[4]+2]=(IG_V_N-(buf_VTG[Comma_Num[4]+1]-0x30)*100)/10+0x30;
		buf_VTG[Comma_Num[4]+3]=IG_V_N-(buf_VTG[Comma_Num[4]+1]-0x30)*100-(buf_VTG[Comma_Num[4]+2]-0x30)*10+0x30;
		buf_VTG[Comma_Num[4]+4]='.';
		buf_VTG[Comma_Num[4]+5]=(IG_V_N-(buf_VTG[Comma_Num[4]+1]-0x30)*100-(buf_VTG[Comma_Num[4]+2]-0x30)*10-(buf_VTG[Comma_Num[4]+3]-0x30))*10+0x30;	
		
		buf_VTG[Comma_Num[6]+1]=IG_V_K/1000+0x30;
		buf_VTG[Comma_Num[6]+2]=(IG_V_K-(buf_VTG[Comma_Num[6]+1]-0x30)*1000)/100+0x30;
		buf_VTG[Comma_Num[6]+3]=IG_V_K-(buf_VTG[Comma_Num[6]+1]-0x30)*1000-(buf_VTG[Comma_Num[6]+2]-0x30)*100+0x30;
		buf_VTG[Comma_Num[6]+4]=IG_V_K-(buf_VTG[Comma_Num[6]+1]-0x30)*1000-(buf_VTG[Comma_Num[6]+2]-0x30)*100-(buf_VTG[Comma_Num[6]+3]-0x30)*10+0x30;
		buf_VTG[Comma_Num[6]+5]='.';
		buf_VTG[Comma_Num[6]+6]=(IG_V_K-(buf_VTG[Comma_Num[6]+1]-0x30)*1000-(buf_VTG[Comma_Num[6]+2]-0x30)*100-(buf_VTG[Comma_Num[6]+3]-0x30)*10-(buf_VTG[Comma_Num[6]+4]-0x30))*10+0x30;	
		//GPS�ٶȸ�ֵ
	 if(GNSSAdvInfo.SatNum>=4)
	 {
		 if((GNSSAdvInfo.Angle_T>=0)&&(GNSSAdvInfo.Angle_T<=90))	  //��һ����
		 {
				GNSSAdvInfo.Vn=(GNSSAdvInfo.V_K/3.6f)*cos((float32_t)GNSSAdvInfo.Angle_T/57.3f);   //GPS �����ٶ�
				GNSSAdvInfo.Ve=(GNSSAdvInfo.V_K/3.6f)*sin((float32_t)GNSSAdvInfo.Angle_T/57.3f);   //GPS �����ٶ�
		 }
		 else if((GNSSAdvInfo.Angle_T>90)&&(GNSSAdvInfo.Angle_T<=180))	  //�ڶ�����
		 {
				GNSSAdvInfo.Vn=-(GNSSAdvInfo.V_K/3.6f)*sin(PI-(float32_t)GNSSAdvInfo.Angle_T/57.3f);   //GPS �����ٶ�
				GNSSAdvInfo.Ve=(GNSSAdvInfo.V_K/3.6f)*cos(PI-(float32_t)GNSSAdvInfo.Angle_T/57.3f);   //GPS �����ٶ�
		 }
		 else if((GNSSAdvInfo.Angle_T>180)&&(GNSSAdvInfo.Angle_T<=270))	 //��������
		 {
				GNSSAdvInfo.Vn=-(GNSSAdvInfo.V_K/3.6f)*cos((float32_t)GNSSAdvInfo.Angle_T/57.3f-PI);   //GPS �����ٶ�
				GNSSAdvInfo.Ve=-(GNSSAdvInfo.V_K/3.6f)*sin((float32_t)GNSSAdvInfo.Angle_T/57.3f-PI);   //GPS �����ٶ�
		 }
		 else if((GNSSAdvInfo.Angle_T>270)&&(GNSSAdvInfo.Angle_T<360))	 //��������
		 {
				GNSSAdvInfo.Vn=(GNSSAdvInfo.V_K/3.6f)*sin((float32_t)GNSSAdvInfo.Angle_T/57.3f-1.5f*PI);   //GPS �����ٶ�
				GNSSAdvInfo.Ve=-(GNSSAdvInfo.V_K/3.6f)*cos((float32_t)GNSSAdvInfo.Angle_T/57.3f-1.5f*PI);   //GPS �����ٶ�
		 }	
	 }
	}       
}

