/***************************************************************** 
*  @brief:          
*  @Function:   
*  @Author:       
*  @Date:         
*  @CopyRight:
*  @Version:     
*  @Description:
*****************************************************************/
#include "BaroRead.h"

#define MS5611Press_OSR  MS561101BA_OSR_4096  
#define MS5611Temp_OSR   MS561101BA_OSR_4096  

// ??????
#define SCTemperature    0x01	  
#define CTemperatureing  0x02  
#define SCPressure       0x03	 
#define SCPressureing    0x04	 

#define MOVAVG_SIZE  10	   

const float  MS5611_Lowpass = 7.9577e-3f;  //10hz

static uint8_t  Now_doing = SCTemperature;	
static uint16_t PROM_C[MS561101BA_PROM_REG_COUNT]; 
static uint32_t Current_delay;	   //?????? us 
static uint32_t Start_Convert_Time;   //?????? ?? us 
static int32_t  tempCache;
uint8_t ALT_Updated = 0; //????????????
static float Alt_Offset_cm = 0;
static float avg_Pressure;
float ALT_Update_Interval = 0.0; //??????,???????
extern float Alt_offset_Pa;
//units (Celsius degrees*100, mbar*100  ).
//?? [?? 0.01?] [?? ?]  [??0.01?] 
float MS5611_Temperature,MS5611_Pressure,MS5611_Altitude;
// ???  ?? us 	  ???????????????
uint32_t MS5611_Delay_us[9] = {
	1500,//MS561101BA_OSR_256 0.9ms  0x00
	1500,//MS561101BA_OSR_256 0.9ms  
	2000,//MS561101BA_OSR_512 1.2ms  0x02
	2000,//MS561101BA_OSR_512 1.2ms
	3000,//MS561101BA_OSR_1024 2.3ms 0x04
	3000,//MS561101BA_OSR_1024 2.3ms
	5000,//MS561101BA_OSR_2048 4.6ms 0x06
	5000,//MS561101BA_OSR_2048 4.6ms
	11000,//MS561101BA_OSR_4096 9.1ms 0x08
};

// FIFO ??					
static float Temp_buffer[MOVAVG_SIZE],Press_buffer[MOVAVG_SIZE],Alt_buffer[MOVAVG_SIZE];
static uint8_t temp_index=0,press_index=0; //????
float Alt_offset_Pa=0; //???0?? ??????  ???????????? 
uint8_t  Covert_count=0;

//???????? ???? ????
void MS561101BA_NewTemp(float val) {
  Temp_buffer[temp_index] = val;
  temp_index = (temp_index + 1) % MOVAVG_SIZE;
}

//???????? ???? ????
void MS561101BA_NewPress(float val) {
  Press_buffer[press_index] = val;
  press_index = (press_index + 1) % MOVAVG_SIZE;
}

//???????? ???? ????
void MS561101BA_NewAlt(float val) 
{
  int16_t i;
  static uint32_t alt_lastupdate , temp;
  temp = micros();
  ALT_Update_Interval = ((float)(temp - alt_lastupdate))/1000000.0f;
  alt_lastupdate = temp;
  for(i=1;i<MOVAVG_SIZE;i++)
  Alt_buffer[i-1] = Alt_buffer[i];
  Alt_buffer[MOVAVG_SIZE-1] = val;
}

//?????D???
float MS5611BA_Get_D(void)
{
	float new=0,old=0;
	int16_t i;
	for(i=0;i<MOVAVG_SIZE/2;i++)
		old += Alt_buffer[i];
	old /= (MOVAVG_SIZE/2);

	for(i=MOVAVG_SIZE/2;i<MOVAVG_SIZE;i++)
	    new += Alt_buffer[i];
	new /= (MOVAVG_SIZE/2);

	return new - old;
}

//???? ????
float MS561101BA_getAvg(float * buff, int size) 
{
  float sum = 0.0;
  int i;
  for(i=0; i<size; i++) {
    sum += buff[i];
  }
  return sum / size;
}

/**************************????********************************************
*????:		void MS561101BA_readPROM(void)
*?  ?:	    ?? MS561101B ??????
?? ???????  ????????????
*******************************************************************************/
void MS561101BA_readPROM(void) 
{
  u8  inth,intl;
  int i;
  for (i=0;i<MS561101BA_PROM_REG_COUNT;i++) {
		IIC_Start();
    IIC_Send_Byte(MS5611_ADDR);
		IIC_Wait_Ack();
    IIC_Send_Byte(MS561101BA_PROM_BASE_ADDR + (i * MS561101BA_PROM_REG_SIZE));
		IIC_Wait_Ack();	
    IIC_Stop();
		delay_us(5);
   	IIC_Start();
		IIC_Send_Byte(MS5611_ADDR+1);  //??????	
		delay_us(1);
		IIC_Wait_Ack();
		inth = IIC_Read_Byte(1);  //?ACK????
		delay_us(1);
		intl = IIC_Read_Byte(0);	 //??????NACK
		IIC_Stop();
    PROM_C[i] = (((uint16_t)inth << 8) | intl);
  }
}

/**************************????********************************************
*????:		void MS561101BA_reset(void)
*?  ?:	    ??????? MS561101B 
*******************************************************************************/
void MS561101BA_reset(void) 
{
	IIC_Start();
  IIC_Send_Byte(MS5611_ADDR); //???
	IIC_Wait_Ack();
  IIC_Send_Byte(MS561101BA_RESET);//??????
	IIC_Wait_Ack();	
  IIC_Stop();
}

/**************************????********************************************
*????:		void MS561101BA_startConversion(uint8_t command)
*?  ?:	    ????????? MS561101B
??? ????? MS561101BA_D1  ????
				  MS561101BA_D2  ????	 
*******************************************************************************/
void MS561101BA_startConversion(uint8_t command) 
{
  // initialize pressure conversion
  IIC_Start();
  IIC_Send_Byte(MS5611_ADDR); //???
  IIC_Wait_Ack();
  IIC_Send_Byte(command); //?????
  IIC_Wait_Ack();	
  IIC_Stop();
}

/**************************????********************************************
*????:		unsigned long MS561101BA_getConversion(void)
*?  ?:	    ?? MS561101B ?????	 
*******************************************************************************/
unsigned long MS561101BA_getConversion(void) 
{
		unsigned long conversion = 0;
		u8 temp[3];
		IIC_Start();
    IIC_Send_Byte(MS5611_ADDR); //???
		IIC_Wait_Ack();
    IIC_Send_Byte(0);// start read sequence
		IIC_Wait_Ack();	
    IIC_Stop();
		
		IIC_Start();
		IIC_Send_Byte(MS5611_ADDR+1);  //??????	
		IIC_Wait_Ack();
		temp[0] = IIC_Read_Byte(1);  //?ACK????  bit 23-16
		temp[1] = IIC_Read_Byte(1);  //?ACK????  bit 8-15
		temp[2] = IIC_Read_Byte(0);  //?NACK???? bit 0-7
		IIC_Stop();
		conversion = (unsigned long)temp[0] * 65536 + (unsigned long)temp[1] * 256 + (unsigned long)temp[2];
		return conversion;
}


/**************************????********************************************
*????:		void MS561101BA_GetTemperature(void)
*?  ?:	    ?? ??????	 
*******************************************************************************/
void MS561101BA_GetTemperature(void)
{	
	tempCache = MS561101BA_getConversion();	
}
/**************************????********************************************
*????:		float MS561101BA_get_altitude(void)
*?  ?:	    ????????? ???	 
*******************************************************************************/
float MS561101BA_get_altitude(void)
{

	static float Altitude;
	if(Alt_offset_Pa==0)
	{ // ??????0?????
		if(Covert_count++<50);  //?????? ? ?????????
		else Alt_offset_Pa = MS5611_Pressure; //? ???????? 0 ?????
		avg_Pressure = MS5611_Pressure;
		Altitude = 0; //?? ? 0
		return Altitude;
	}
	//????? ??????? ??? ?
	Altitude = 4433000.0 * (1 - pow((MS5611_Pressure / Alt_offset_Pa), 0.1903));
	Altitude = Altitude + Alt_Offset_cm ;  //???
	MS561101BA_NewAlt(Altitude);
	Altitude = MS561101BA_getAvg(Alt_buffer,MOVAVG_SIZE);
	return (Altitude);
}

/**************************????********************************************
*????:		void MS561101BA_ResetAlt(void)
*?  ?:	    ????????0??????	 
*******************************************************************************/
void MS561101BA_ResetAlt(void){
	Alt_offset_Pa = MS5611_Pressure; //? ???????? 0 ?????	
	Alt_Offset_cm = 0;
}

/**************************????********************************************
*????:		void MS561101BA_SetAlt(void)
*?  ?:	    ???????? Current ??????	 
*******************************************************************************/
void MS561101BA_SetAlt(float Current){
	Alt_offset_Pa = (avg_Pressure); //? ???????? 0 ?????	
	Alt_Offset_cm = Current*100.0f; //??? CM
	MS561101BA_NewAlt(Current*100.0f);	 //?????
	ALT_Updated = 1; //???? ???
}

/**************************????********************************************
*????:		void MS561101BA_getPressure(void)
*?  ?:	    ?? ?????? ??????	 
*******************************************************************************/
void MS561101BA_getPressure(void) {
	int64_t off,sens;
	int64_t TEMP1,T2,Aux_64,OFF2,SENS2;  // 64 bits
	int32_t rawPress = MS561101BA_getConversion();
	int64_t dT  = tempCache - (((int32_t)PROM_C[4]) << 8);
	float temp;
	TEMP1 = 2000 + (dT * (int64_t)PROM_C[5])/8388608;
	off  = (((int64_t)PROM_C[1]) << 16) + ((((int64_t)PROM_C[3]) * dT) >> 7);
	sens = (((int64_t)PROM_C[0]) << 15) + (((int64_t)(PROM_C[2]) * dT) >> 8);
	
	if (TEMP1 < 2000)
	{   // second order temperature compensation
		T2 = (((int64_t)dT)*dT) >> 31;
		Aux_64 = (TEMP1-2000)*(TEMP1-2000);
		OFF2 = (5*Aux_64)>>1;
		SENS2 = (5*Aux_64)>>2;
		TEMP1 = TEMP1 - T2;
		off = off - OFF2;
		sens = sens - SENS2;
	}

	MS561101BA_NewPress((((((int64_t)rawPress) * sens) >> 21) - off) / 32768);
  MS5611_Pressure = MS561101BA_getAvg(Press_buffer,MOVAVG_SIZE); //0.01mbar
	
	avg_Pressure = avg_Pressure + (MS5611_Pressure - avg_Pressure)*0.1f;

	MS561101BA_NewTemp(TEMP1);
	MS5611_Temperature = MS561101BA_getAvg(Temp_buffer,MOVAVG_SIZE); //0.01c
	
	temp = MS561101BA_get_altitude(); // 0.01meter
							  
	MS5611_Altitude =MS5611_Altitude +(ALT_Update_Interval/(ALT_Update_Interval + MS5611_Lowpass))*(temp - MS5611_Altitude); //????   20hz
	
}


/**************************????********************************************
*????:		void MS5611BA_Routing(void)
*?  ?:	    MS5611BA ????? ,?????? ?????????? 	 
*******************************************************************************/
void MS5611BA_Routing(void) {

	switch(Now_doing)
		{ 
		case SCTemperature:  
		{
			MS561101BA_startConversion(MS561101BA_D2 + MS5611Temp_OSR);
			Current_delay = MS5611_Delay_us[MS5611Temp_OSR] ;
			Start_Convert_Time = micros(); //????
			Now_doing = CTemperatureing;//?????
		}
			break;
		case CTemperatureing:  //????? 
		{
			if((micros()-Start_Convert_Time) > Current_delay)
			{
			MS561101BA_GetTemperature(); //???	
			Now_doing = SCPressure;	
			}
		}
			break;
		case SCPressure:
		{
			MS561101BA_startConversion(MS561101BA_D1 + MS5611Press_OSR);
			Current_delay = MS5611_Delay_us[MS5611Press_OSR];//????
			Start_Convert_Time = micros();//????
			Now_doing = SCPressureing;//?????
		}
			break;
		case SCPressureing:	 //???????
		{
			if((micros()-Start_Convert_Time) > Current_delay)
			{ 
				MS561101BA_getPressure();   	
				ALT_Updated = 1; 
				Now_doing = SCTemperature; 		
			}
		}
			break;
		default: 
			Now_doing = SCTemperature;
			break;
	}
}


