/***************************************************************** 
*  @brief:        
*  @File:          
*  @Project:    
*  @Author:      
*  @Date:         
*  @CopyRight:   
*  @Version:      
*  @Description:  
*****************************************************************/
#include "ExtDevCon.h"

void ExtDevCon_Init(void){
  ExtDev_Par.BufferData = Command_Par_Info[COMMAND_DEVCON].Buffer;
  Modbus_Solve.Info->StaAdd      = (uint8_t) ExtDev_Par.BufferData[0];
  Modbus_Solve.Info->FuncCode    = (uint8_t) ExtDev_Par.BufferData[1];
  Modbus_Solve.Info->DataLength  = (uint8_t) ExtDev_Par.BufferData[2];  
}

