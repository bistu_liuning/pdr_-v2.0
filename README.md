# PDR_V2.0

## 通知
1. ZUPT+ZARU+MAG程序见：https://gitee.com/bistu_liuning/nav-os-v2.0/tree/master/Matlab/NavOS_Matlab  
2. 上述程序还包含腰部利用步长和航向计算位置的程序。
3. 首批开放Matlab版本程序。
4. 如需硬件电路（收费），也请邮箱联系。

## 本次更新内容
1. 更新MPU9255版电路图程序及相关图纸。
2. NavOS中的Matlab程序即为PDR的基本版程序，接入数据后可直接运行。相关连接见：https://gitee.com/bistu_liuning/nav-os-v2.0/tree/master/Matlab/NavOS_Matlab
3. 上传Python版程序。

